﻿namespace Twitter.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text.RegularExpressions;

    public static class CoreExtensions
    {
        public static int ToInt32(this object value, int defaultValue = -1)
        {
            if (value == null)
                return defaultValue;

            int output = defaultValue;
            if (!int.TryParse(value.ToString(), out output))
                return defaultValue;

            return output;
        }

        public static DateTime ToUnixTimeToDateTime(this string timestamp)
        {
            long convertThis = long.Parse(timestamp);

            var dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dateTime = dateTime.AddSeconds((double)convertThis);
            dateTime = dateTime.ToLocalTime();  // Change GMT time to your timezone
            return dateTime;
        }

        public static Int32 ToDateTimeToUnixTime(this DateTime datetime)
        {
            int unixTimestamp = (int)(datetime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            return unixTimestamp;
        }

        /// <summary>
        /// Datetime to Time ago string
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToTimeAgo(this DateTime dt)
        {
            TimeSpan span = DateTime.Now - dt;
            if (span.Days > 365)
            {
                int years = (span.Days / 365);
                if (span.Days % 365 != 0)
                    years += 1;
                return String.Format("{0}y", years);
            }
            if (span.Days > 7)
                return String.Format("{0}w", (int)(span.Days / 7));
            if (span.Days < 7 && span.Days > 0)
                return String.Format("{0}d", span.Days);
            if (span.Hours > 0)
                return String.Format("{0}h", span.Hours);
            if (span.Minutes > 0)
                return String.Format("{0}m", span.Minutes);
            if (span.Seconds > 5)
                return String.Format("{0}s", span.Seconds);
            if (span.Seconds <= 5)
                return "0s";
            return string.Empty;
        }

        public static List<string> ToHashtags(this string text)
        {
            var list = new List<string>();
            var regex = new Regex(@"[#](\W*[a-zA-Z0-9_ğüşıöçĞÜŞİÖÇ]\w*)");
            var matches = regex.Matches(text);

            foreach (Match m in matches)
            {
                if (m.Value.Contains("&"))
                    continue;

                list.Add(m.Value);
            }

            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static List<string> ToMentions(this string value)
        {
            var list = new List<string>();
            var regex = new Regex(@"[@](\W*[a-zA-Z0-9_.ğüşıöçĞÜŞİÖÇ]\w*)");
            var matches = regex.Matches(value);

            foreach (Match m in matches)
            {
                if (!list.Contains(m.Value))
                    list.Add(m.Value.Replace("(", "").Replace(")", "").Replace("*", "").Replace("/", "").Replace("\\", ""));
            }
            return list;
        }

        public static string ClearCharsForLinks(this string input)
        {
            string output = input
                .Replace("%", "")
                .Replace("%25", "")
                .Replace("<", "")
                .Replace("%3C", "")
                .Replace("%3c", "")
                .Replace("%7C", "")
                .Replace("%7c", "")
                .Replace("|", "")
                .Replace(">", "")
                .Replace("%3E", "")
                .Replace("%3e", "")
                .Replace("*", "")
                .Replace("%2A", "")
                .Replace(":", "")
                .Replace("%3A", "")
                .Replace("&", "")
                .Replace("%26", "")
                .Replace("/", "-")
                .Replace("%0A", "")
                .Replace("~", "")
                .Replace(".", "")
                .Replace("%5C", "")
                .Replace("%5c", "")
                .Replace("\\", "")
                .Replace("%20", "")
                .Replace("%22", "")
                .Replace("%3f", "")
                .Replace("%3F", "")
                .Replace("?", "")
                .Replace("\"", "");


            output = output.Trim(' ');

            return output;
        }

        /// <summary>
        /// Convert integers to nice number strings
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static string ToNiceNumber(this int num)
        {
            // Ensure number has max 3 significant digits (no rounding up can happen)
            int i = (int)Math.Pow(10, (int)Math.Max(0, Math.Log10(num) - 2));
            num = num / i * i;

            if (num >= 1000000000)
                return (num / 1000000000D).ToString("0.##") + "B";
            if (num >= 1000000)
                return (num / 1000000D).ToString("0.##") + "M";
            if (num >= 1000)
                return (num / 1000D).ToString("0.##") + "K";

            return num.ToString("#,0");
        }

        /// <summary>
        /// Convert integers to nice number strings
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static string ToNiceNumber(this long num)
        {
            // Ensure number has max 3 significant digits (no rounding up can happen)
            int i = (int)Math.Pow(10, (int)Math.Max(0, Math.Log10(num) - 2));
            num = num / i * i;

            if (num >= 1000000000)
                return (num / 1000000000D).ToString("0.##") + "B";
            if (num >= 1000000)
                return (num / 1000000D).ToString("0.##") + "M";
            if (num >= 1000)
                return (num / 1000D).ToString("0.##") + "K";

            return num.ToString("#,0");
        }

        public static string ClearUnwantedChars(this string input)
        {
            string output = input
                .Replace("%", "")
                .Replace("%25", "")
                .Replace("<", "")
                .Replace("%3C", "")
                .Replace("%3c", "")
                .Replace(">", "")
                .Replace("%3E", "")
                .Replace("%3e", "")
                .Replace("*", "")
                .Replace("%2A", "")
                .Replace(":", "")
                .Replace("%3A", "")
                .Replace("&", "")
                .Replace("%26", "")
                .Replace("/", "-")
                .Replace("%0A", "")
                .Replace("~", "")
                .Replace(".", "")
                .Replace("%5C", "")
                .Replace("%5c", "");

            return output;
        }

        public static string ToSlug(this string phrase)
        {
            //clear punctuation
            string str = phrase.ClearPunctuation().ToLowerInvariant();

            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();

            // cut and trim 
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", ""); //hyphens  

            return str;
        }

        public static string ToStripHtml(this string value)
        {
            string pattern = @"<(.|\n)*?>";

            return Regex.Replace(value, pattern, string.Empty);
        }

        public static string ClearLineBreaks(this string value)
        {
            value = value.Replace("\n", "");
            value = value.Replace(Environment.NewLine, "");

            return value;
        }

        private static string ClearPunctuation(this string value)
        {
            var list = new List<Char>();
            foreach (char c in value)
            {
                if (Char.IsPunctuation(c))
                    continue;

                list.Add(c);
            }

            value = string.Concat(list.ToArray());

            return value;
        }

        public static string ToTrim(this string value, int length)
        {
            if (value.Length < length)
                return value;

            return value.Substring(0, length - 1);
        }

        public static string ToLowerCulture(this string value)
        {
            return value.ToLower(new CultureInfo("en-US"));
        }

        public static string ClearForJson(this string value)
        {
            return value.Replace("\"", "");
        }
    }
}
