﻿namespace Twitter.Extensions
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System;

    public static class JsonExtensions
    {
        public static string MapToString(this JObject jObj, string key)
        {
            if (jObj == null || jObj[key] == null)
                return "";

            return jObj[key].ToString();
        }

        public static bool MapToBool(this JObject jObj, string key, bool defaultValue = false)
        {
            if (jObj == null || jObj[key] == null)
                return defaultValue;

            bool val = defaultValue;
            if (!bool.TryParse(jObj[key].ToString(), out val))
                val = defaultValue;

            return val;
        }

        public static int MapToInt(this JObject jObj, string key, int defaultValue = 0)
        {
            if (jObj == null || jObj[key] == null)
                return defaultValue;

            int val = defaultValue;
            if (!int.TryParse(jObj[key].ToString(), out val))
                val = defaultValue;

            return val;
        }

        public static long MapToLong(this JObject jObj, string key, long defaultValue = 0)
        {
            if (jObj == null || jObj[key] == null)
                return defaultValue;

            long val = defaultValue;
            if (!long.TryParse(jObj[key].ToString(), out val))
                val = defaultValue;

            return val;
        }

        public static DateTime MapToDatetime(this JObject jObj, string key, DateTime defaultValue)
        {
            if (jObj == null || jObj[key] == null)
                return defaultValue;

            DateTime val = defaultValue;
            if (!DateTime.TryParse(jObj[key].ToString(), out val))
                val = defaultValue;

            return val;
        }

        public static bool IsJsonValid(this string jsonStr)
        {
            try
            {
                JToken.Parse(jsonStr);
                return true;
            }
            catch (JsonReaderException ex)
            {
                return false;
            }
        }
    }
}
