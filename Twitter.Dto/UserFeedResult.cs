﻿namespace Twitter.Dto
{
    using System.Collections.Generic;
    using Base;

    public class UserFeedResult : BaseResult
    {
        public User User { get; set; }
        public List<Status> Statuses { get; set; }
        public string LastId { get; set; }
        public string FirstId { get; set; }
        public bool IsFirstPage { get; set; }

        public bool IsAdsEnabled { get; set; }
    }
}
