﻿namespace Twitter.Dto.Base
{
    public class Trend
    {
        public string Name { get; set; }
        public string Query { get; set; }
        public int Volume { get; set; }
    }
}
