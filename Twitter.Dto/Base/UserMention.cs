﻿namespace Twitter.Dto.Base
{
    public class UserMention
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ScreenName { get; set; }
    }
}
