﻿namespace Twitter.Dto.Base
{
    using System;
    using System.Collections.Generic;

    public class User
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ScreenName { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public string ProfileUrl { get; set; }
        public string Url { get; set; }
        public bool Protected { get; set; }
        public int FollowersCount { get; set; }
        public int FriendsCount { get; set; }
        public DateTime CreatedAt { get; set; }
        public int FavouritesCount { get; set; }
        public bool Verified { get; set; }
        public int StatusesCount { get; set; }
        public string Lang { get; set; }
        public bool IsTranslator { get; set; }
        public string BackgroundColor { get; set; }
        public string BackgroundImageUrl { get; set; }
        public string BackgroundImageUrlHttps { get; set; }
        public string BackgroundTile { get; set; }
        public string ImageUrl { get; set; }
        public string ImageUrlHttps { get; set; }
        public string BannerUrl { get; set; }
        public string LinkColor { get; set; }
        public string SidebarBorderColor { get; set; }
        public string SidebarFillColor { get; set; }
        public string TextColor { get; set; }
        public bool DefaultProfile { get; set; }
        public bool DefaultProfileImage { get; set; }
    }

    public class GenericEqualityCompare<T> : IEqualityComparer<T>
    {
        private Func<T, object> _expr { get; set; }
        public GenericEqualityCompare(Func<T, object> expr)
        {
            this._expr = expr;
        }
        public bool Equals(T x, T y)
        {
            var first = _expr.Invoke(x);
            var sec = _expr.Invoke(y);
            if (first != null && first.Equals(sec))
                return true;
            else
                return false;
        }
        public int GetHashCode(T obj)
        {
            return obj.GetHashCode();
        }
    }
}
