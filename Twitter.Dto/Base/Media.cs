﻿namespace Twitter.Dto.Base
{
    public class Media
    {
        public string Id { get; set; }
        public string MediaUrl { get; set; }
        public string MediaUrlHttps { get; set; }
        public string Url { get; set; }
        public string ExpandedUrl { get; set; }
        public string Type { get; set; }
        public string SourceStatusId { get; set; }
        public string SourceUserId { get; set; }
        public string VideoUrl { get; set; }
    }
}
