﻿namespace Twitter.Dto.Base
{
    using System;
    using System.Collections.Generic;

    public class Retweeted
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public User User { get; set; }
        public int RetweetCount { get; set; }
        public int FavoriteCount { get; set; }
        public string Lang { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<string> Hashtags { get; set; }
        public List<string> Urls { get; set; }
        public List<UserMention> UserMentions { get; set; }
        public List<Media> Medias { get; set; }
        public Place Place { get; set; }
        public string TimeAgo { get; set; }
    }
}
