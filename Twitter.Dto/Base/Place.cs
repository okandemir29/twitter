﻿namespace Twitter.Dto.Base
{
    public class Place
    {
        public string Id { get; set; }
        public string PlaceType { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string CountryCode { get; set; }
        public string Country { get; set; }
    }
}
