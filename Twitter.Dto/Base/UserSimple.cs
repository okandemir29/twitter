﻿namespace Twitter.Dto.Base
{
    public class UserSimple
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ScreenName { get; set; }
        public string Description { get; set; }
        public bool Protected { get; set; }
        public int FollowersCount { get; set; }
        public int FriendsCount { get; set; }
        public string ImageUrl { get; set; }
    }
}
