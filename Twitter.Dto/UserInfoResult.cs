﻿namespace Twitter.Dto
{
    using Twitter.Dto.Base;

    public class UserInfoResult : BaseResult
    {
        public User User { get; set; }
    }
}
