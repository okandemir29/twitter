﻿namespace Twitter.Dto
{
    using System.Collections.Generic;
    using Base;

    public class TagFeedResult : BaseResult
    {
        public List<Status> Statuses { get; set; }
        public string Tag { get; set; }
        public string LastId { get; set; }
        public string FirstId { get; set; }
        public bool IsFirstPage { get; set; }
    }
}
