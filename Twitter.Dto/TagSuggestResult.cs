﻿namespace Twitter.Dto
{
    using System.Collections.Generic;

    public class TagSuggestResult : BaseResult
    {
        public List<SearchTag> Tags { get; set; }
    }

    public class SearchTag
    {
        public string Tag { get; set; }
        public int MediaCount { get; set; }
    }
}
