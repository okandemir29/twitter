﻿namespace Twitter.Dto
{
    public class BaseResult
    {
        public bool IsSucceed { get; set; }
        public string Message { get; set; }
    }
}
