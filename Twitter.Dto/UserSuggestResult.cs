﻿namespace Twitter.Dto
{
    using System.Collections.Generic;
    using Base;

    public class UserSuggestResult : BaseResult
    {
        public User[] Users { get; set; }
    }
}
