﻿namespace Twitter.Dto
{
    using System.Collections.Generic;
    using Twitter.Dto.Base;

    public class UserFriendshipResult : BaseResult
    {
        public User User { get; set; }
        public List<UserSimple> Users { get; set; }
    }
}
