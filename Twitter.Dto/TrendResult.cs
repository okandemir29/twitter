﻿namespace Twitter.Dto
{
    using System.Collections.Generic;
    using Twitter.Dto.Base;

    public class TrendResult : BaseResult
    {
        public List<Trend> Trends { get; set; }
    }
}
