﻿namespace Twitter.Dto
{
    using Base;

    public class StatusResult : BaseResult
    {
        public Status Status { get; set; }
    }
}
