﻿namespace Twitter.Data
{
    using Twitter.Data.Core;

    public class BanwordData : Core.EntityBaseData<Model.Banword>
    {
        public BanwordData() : base(new DataContext()) { }
    }
}
