﻿namespace Twitter.Data
{
    using Twitter.Data.Core;

    public class PopularUserData : Core.EntityBaseData<Model.PopularUser>
    {
        public PopularUserData() : base(new DataContext()) { }
    }
}
