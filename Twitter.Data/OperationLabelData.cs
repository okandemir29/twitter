﻿namespace Twitter.Data
{
    using Twitter.Data.Core;

    public class OperationLabelData : Core.EntityBaseData<Model.OperationLabel>
    {
        public OperationLabelData() : base(new DataContext()) { }
    }
}
