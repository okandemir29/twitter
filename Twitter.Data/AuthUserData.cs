﻿namespace Twitter.Data
{
    using System.Collections.Generic;
    using Twitter.Data.Core;

    public class AuthUserData : Core.EntityBaseData<Model.AuthUser>
    {
        public AuthUserData() : base(new DataContext()) { }

        public List<AuthGethererInfos> GetTokens()
        {
            var list = new List<AuthGethererInfos>();

            var users = new AuthUserData().GetBy(x => x.IsActive);
            
            if(users.Count > 0)
            {
                foreach (var item in users)
                {
                    var token = new TokenData().GetByKey(item.TokenId);

                    var model = new AuthGethererInfos()
                    {
                        ConsumerKey = token.ConsumerKey,
                        ConsumerSecret = token.ConsumerSecret,
                        OAuthToken = item.OAuthToken,
                        OAuthTokenSecret = item.OAuthTokenSecret,
                    };

                    list.Add(model);
                }

                return list;
            }

            return list;
        }
    }
}
