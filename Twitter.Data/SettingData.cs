﻿namespace Twitter.Data
{
    using Twitter.Data.Core;

    public class SettingData : Core.EntityBaseData<Model.Setting>
    {
        public SettingData() : base(new DataContext()) { }
    }
}
