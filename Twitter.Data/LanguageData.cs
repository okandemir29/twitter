﻿namespace Twitter.Data
{
    using Twitter.Data.Core;

    public class LanguageData : Core.EntityBaseData<Model.Language>
    {
        public LanguageData() : base(new DataContext()) { }
    }
}
