﻿namespace Twitter.Data
{
    using Twitter.Data.Core;

    public class OperationValueData : Core.EntityBaseData<Model.OperationValue>
    {
        public OperationValueData() : base(new DataContext()){}
    }
}
