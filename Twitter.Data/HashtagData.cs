﻿namespace Twitter.Data
{
    using Twitter.Data.Core;

    public class HashtagData : Core.EntityBaseData<Model.Hashtag>
    {
        public HashtagData() : base(new DataContext()) { }
    }
}
