﻿namespace Twitter.Data
{
    using Twitter.Data.Core;

    public class TokenData : Core.EntityBaseData<Model.Token>
    {
        public TokenData() : base(new DataContext()) { }
    }
}
