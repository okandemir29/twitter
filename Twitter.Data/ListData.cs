﻿namespace Twitter.Data
{
    using Twitter.Data.Core;

    public class ListData : Core.EntityBaseData<Model.List>
    {
        public ListData() : base(new DataContext()) { }
    }
}
