﻿namespace Twitter.Data
{
    using Twitter.Data.Core;

    public class BanUserData : Core.EntityBaseData<Model.BanUser>
    {
        public BanUserData() : base(new DataContext()) { }
    }
}
