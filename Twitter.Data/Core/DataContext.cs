﻿namespace Twitter.Data.Core
{
    using Twitter.Model;
    using System.Data.Entity;

    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class DataContext : DbContext
    {
        public DataContext() : base("MyDbContextConnectionString")
        {
        }

        public DbSet<Token> Tokens { get; set; }
        public DbSet<AuthUser> AuthUsers { get; set; }
        public DbSet<List> Lists { get; set; }
        public DbSet<PopularUser> PopularUsers { get; set; }
        public DbSet<Stat> Stats { get; set; }
        public DbSet<Banword> Banwords { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<BanUser> BanUsers { get; set; }
        public DbSet<Hashtag> Hashtags { get; set; }

        //Dil tabloları
        public DbSet<OperationLabel> OperationLabels { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<OperationValue> OperationValues { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Token>().ToTable("twi_Tokens");
            modelBuilder.Entity<AuthUser>().ToTable("twi_AuthUsers");
            modelBuilder.Entity<List>().ToTable("twi_Lists");
            modelBuilder.Entity<PopularUser>().ToTable("twi_PopularUsers");
            modelBuilder.Entity<Stat>().ToTable("twi_Stats");
            modelBuilder.Entity<Banword>().ToTable("twi_Banwords");
            modelBuilder.Entity<Setting>().ToTable("twi_Settings");
            modelBuilder.Entity<BanUser>().ToTable("twi_BanUsers");
            modelBuilder.Entity<Hashtag>().ToTable("twi_Hashtags");

            //Dil tabloları
            modelBuilder.Entity<OperationLabel>().ToTable("twi_OperationLabels");
            modelBuilder.Entity<Language>().ToTable("twi_Languages");
            modelBuilder.Entity<OperationValue>().ToTable("twi_OperationValues");

            base.OnModelCreating(modelBuilder);
        }
    }
}