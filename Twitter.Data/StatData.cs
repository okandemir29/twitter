﻿namespace Twitter.Data
{
    using Twitter.Data.Core;

    public class StatData : Core.EntityBaseData<Model.Stat>
    {
        public StatData() : base(new DataContext()) { }
    }
}
