﻿namespace Twitter.WebUI.Site.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web;

    public static class UIHelper
    {
        public static string ToSlug(this string phrase)
        {
            if (phrase == null)
                phrase = "";

            //clear punctuation
            string str = phrase.ClearPunctuation().ToLowerInvariant();

            str = str.Replace("ı", "i")
                    .Replace("ğ", "g")
                    .Replace("ö", "o")
                    .Replace("ü", "u")
                    .Replace("ş", "s")
                    .Replace("ş", "s")
                    .Replace("_", "-")
                    .Replace("$", "s")
                    .Replace("%7C", "")
                    .Replace("%2b", "q")
                    .Replace("$", "s")
                    .Replace("|", "")
                    .Replace("/", "")
                    .Replace("+", "");

            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();

            // cut and trim 
            str = Regex.Replace(str, @"\s", "-"); //hyphens  

            return str;
        }

        private static string ClearPunctuation(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return value;

            var list = new List<Char>();
            foreach (char c in value)
            {
                if (c != '_' && Char.IsPunctuation(c))
                    continue;

                list.Add(c);
            }

            value = string.Concat(list.ToArray());

            return value;
        }

        public static string ToTitleCase(this string str)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }

        public static string ToCommaSeparetadNumber(this int str)
        {
            try
            {
                long retValue = str;
                return String.Format("{0:#,##0}", retValue);
            }
            catch (Exception)
            {
            }

            return "";
        }

        public static string ToNiceNumber(this long num)
        {
            try
            {
                // Ensure number has max 3 significant digits (no rounding up can happen)
                long i = (long)Math.Pow(10, (long)Math.Max(0, Math.Log10(num) - 2));
                num = num / i * i;

                if (num >= 1000000000)
                    return (num / 1000000000D).ToString("0.##") + "B";
                if (num >= 1000000)
                    return (num / 1000000D).ToString("0.##") + "M";
                if (num >= 1000)
                    return (num / 1000D).ToString("0.##") + "K";

                return num.ToString("#,0");
            }
            catch (Exception ex)
            {

                return "";
            }

        }

        public static string ToCommaSeparetadNumbeForSubscriber(this string val)
        {
            try
            {
                if (string.IsNullOrEmpty(val) || val == "0")
                {
                    val = "Subscriber count is hidden";
                    return val;
                }

                long num = long.Parse(val);
                // Ensure number has max 3 significant digits (no rounding up can happen)
                long i = (long)Math.Pow(10, (long)Math.Max(0, Math.Log10(num) - 2));
                num = num / i * i;

                if (num >= 1000000000)
                    return (num / 1000000000D).ToString("0.##") + "B";
                if (num >= 1000000)
                    return (num / 1000000D).ToString("0.##") + "M";
                if (num >= 1000)
                    return (num / 1000D).ToString("0.##") + "K";

                return num.ToString("#,0");
            }
            catch (Exception ex)
            {

                return "";
            }

        }
    }
}