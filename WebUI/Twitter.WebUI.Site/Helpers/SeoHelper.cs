﻿

namespace Twitter.WebUI.Site.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public static class SeoHelper
    {
        public static string UserImageAlt(string username, string fullname)
        {
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(fullname))
                return $"{fullname}'s Tweets, Followers, Friends in @{username} Twitter Account";
            else if (!string.IsNullOrEmpty(username) && string.IsNullOrEmpty(fullname))
                return $"{username}'s Tweets, Followers, Friends in @{username} Twitter Account";
            else if (string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(fullname))
                return $"{fullname}'s Tweets, Followers, Friends in @{fullname} Twitter Account";
            else
                return "twitter account";
        }

        public static string MediaTitleAndAlt(string username, string fullname, string text)
        {
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(fullname) && !string.IsNullOrEmpty(text))
                return $"{fullname}'s Medias, on @{username} - {text}";
            else if(!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(fullname) && string.IsNullOrEmpty(text))
                return $"{fullname}'s Medias, on @{username}";
            else
                return $"twitter media {text}";
        }

        public static string UserHrefText(string username, string fullname)
        {
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(fullname))
                return $"{fullname}'s Tweets, Followers, Friends and Daily Stats in @{username} Twitter Account";
            else if (!string.IsNullOrEmpty(username) && string.IsNullOrEmpty(fullname))
                return $"{username}'s Tweets, Followers, Friends and Daily Stats in @{username} Twitter Account";
            else if (string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(fullname))
                return $"{fullname}'s Tweets, Followers, Friends and Daily Stats in @{fullname} Twitter Account";
            else
                return "twitter account";
        }

        public static string HastagHrefText(string tag)
        {
            return $"#{tag} twitter feeds, tweets photos and videos";
        }

        public static string ListTitle(string name)
        {
            if (!string.IsNullOrEmpty(name))
                return $"Best of {name} twitter accounts to follow";
            else
                return "Best twitter account list";
        }
    }
}