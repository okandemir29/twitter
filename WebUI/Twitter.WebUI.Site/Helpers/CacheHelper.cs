﻿namespace Twitter.WebUI.Site.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Caching;
    using Twitter.Data;
    using Twitter.Model;
    using Twitter.WebUI.Site.Models;

    public class CacheHelper
    {
        private static string Settings_CacheKey = "Settings_CacheKey";
        private static string Banword_CacheKey = "Banword_CacheKey";
        private static string Users_CacheKey = "Users_CacheKey";
        private static string AuthUser_CacheKey = "AuthUser_CacheKey";
        private static string Language_CacheKey = "Language_CacheKey";
        private static string BanUser_CacheKey = "BanUser_CacheKey";

        public static Setting Setting
        {
            get
            {
                if (HttpContext.Current.Cache[Settings_CacheKey] == null)
                {
                    var setting = new SettingData().GetByKey(1);

                    HttpContext.Current.Cache.Add(
                          Settings_CacheKey, setting
                        , null
                        , DateTime.Now.AddDays(1)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null);
                }

                return HttpContext.Current.Cache[Settings_CacheKey] as Setting;
            }
        }

        public static List<Banword> Banwords
        {
            get
            {
                if (HttpContext.Current.Cache[Banword_CacheKey] == null)
                {
                    var banwords = new BanwordData().GetAll();

                    HttpContext.Current.Cache.Add(
                          Banword_CacheKey, banwords
                        , null
                        , DateTime.Now.AddDays(1)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null);
                }

                return HttpContext.Current.Cache[Banword_CacheKey] as List<Banword>;
            }
        }

        public static List<BanUser> BanUsers
        {
            get
            {
                if (HttpContext.Current.Cache[BanUser_CacheKey] == null)
                {
                    var banwords = new BanUserData().GetAll();

                    HttpContext.Current.Cache.Add(
                          BanUser_CacheKey, banwords
                        , null
                        , DateTime.Now.AddDays(15)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null);
                }

                return HttpContext.Current.Cache[BanUser_CacheKey] as List<BanUser>;
            }
        }

        public static List<Language> Languages
        {
            get
            {
                if (HttpContext.Current.Cache[Language_CacheKey] == null)
                {
                    var banwords = new LanguageData().GetAll();

                    HttpContext.Current.Cache.Add(
                          Language_CacheKey, banwords
                        , null
                        , DateTime.Now.AddDays(1)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null);
                }

                return HttpContext.Current.Cache[Language_CacheKey] as List<Language>;
            }
        }

        public static List<PopularUser> Users
        {
            get
            {
                if (HttpContext.Current.Cache[Users_CacheKey] == null)
                {
                    var banwords = new PopularUserData().GetBy(x=>x.FollowerCount > 3000000);

                    HttpContext.Current.Cache.Add(
                          Users_CacheKey, banwords
                        , null
                        , DateTime.Now.AddDays(10)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null);
                }

                return HttpContext.Current.Cache[Users_CacheKey] as List<PopularUser>;
            }
        }
        
        public static List<AuthGethererInfos> AuthUsers
        {
            get
            {
                if (HttpContext.Current.Cache[AuthUser_CacheKey] == null)
                {
                    var tokens = new AuthUserData().GetTokens();

                    HttpContext.Current.Cache.Add(
                          AuthUser_CacheKey, tokens
                        , null
                        , DateTime.Now.AddDays(10)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null);
                }

                return HttpContext.Current.Cache[AuthUser_CacheKey] as List<AuthGethererInfos>;
            }
        }
        
        public static void ClearCaches()
        {
            HttpContext.Current.Cache.Remove("Settings_CacheKey");
            HttpContext.Current.Cache.Remove("Banword_CacheKey");
            HttpContext.Current.Cache.Remove("BanUser_CacheKey");
            HttpContext.Current.Cache.Remove("Language_CacheKey");
        }
        
        public static void ClearTokens()
        {
            HttpContext.Current.Cache.Remove("AuthUser_CacheKey");
        }

        public static void ClearPopular()
        {
            HttpContext.Current.Cache.Remove("Users_CacheKey");
        }
    }
}