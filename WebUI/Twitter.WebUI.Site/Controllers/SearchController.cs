﻿namespace Twitter.WebUI.Site.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Twitter.Api;
    using Twitter.Data;
    using Twitter.WebUI.Site.Models;

    public class SearchController : Controller
    {
        AuthUserData _userData;
        TokenData _tokenData;
        PopularUserData _popularUserData;

        public SearchController()
        {
            _userData = new AuthUserData();
            _tokenData = new TokenData();
            _popularUserData = new PopularUserData();
        }

        public ActionResult Index(string param)
        {
            var model = new SearchViewModel();

            if (string.IsNullOrEmpty(param))
                return View(model);

            Random rnd = new Random();
            var userById = _userData.GetBy(x => x.IsActive).OrderBy(x => rnd.Next()).FirstOrDefault();
            var token = _tokenData.GetByKey(userById.TokenId);

            var info = new Gatherer(token.ConsumerKey, token.ConsumerSecret, userById.OAuthToken, userById.OAuthTokenSecret);

            var searchUser = info.GetUsersSearch(param);

            if (searchUser.Users.Count() <= 0)
            {
                model.Populars = _popularUserData.GetBy(x => x.IsActive).OrderBy(x => rnd.Next()).Take(20).ToList();
            }

            model = new SearchViewModel()
            {
                Users = searchUser,
                Param = param,
            };

            return View(model);
        }
    }
}