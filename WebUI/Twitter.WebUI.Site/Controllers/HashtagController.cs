﻿namespace Twitter.WebUI.Site.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Twitter.Api;
    using Twitter.Data;
    using Twitter.WebUI.Site.Models;

    public class HashtagController : Controller
    {
        AuthUserData _userData;
        TokenData _tokenData;
        PopularUserData _popularUserData;

        public HashtagController()
        {
            _userData = new AuthUserData();
            _tokenData = new TokenData();
            _popularUserData = new PopularUserData();
        }

        public ActionResult Index(string hashtag)
        {
            if (string.IsNullOrEmpty(hashtag))
                return RedirectToAction("Index", "Home", new { q = "hashtag-is-empty" });

            Random rnd = new Random();
            var userById = _userData.GetBy(x => x.IsActive).OrderBy(x => rnd.Next()).FirstOrDefault();
            var token = _tokenData.GetByKey(userById.TokenId);

            var info = new Gatherer(token.ConsumerKey, token.ConsumerSecret, userById.OAuthToken, userById.OAuthTokenSecret);

            var hashtagResult = info.GetStatusesByTag(hashtag);

            var tags = new HashtagData().GetAll().OrderBy(x => rnd.Next()).Take(15).ToList();

            var model = new HashtagViewModoel()
            {
                Hashtags = hashtagResult,
                TrendHashtags = tags,
            };

            return View(model);
        }
    }
}