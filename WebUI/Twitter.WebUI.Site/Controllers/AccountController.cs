﻿namespace Twitter.WebUI.Site.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Twitter.Api;
    using Twitter.Data;
    using Twitter.Model;
    using Twitter.WebUI.Site.Helpers;
    using Twitter.WebUI.Site.Models;

    public class AccountController : Controller
    {
        StatData _statData;
        PopularUserData _popularUserData;

        public AccountController()
        {
            _statData = new StatData();
            _popularUserData = new PopularUserData();
        }

        public ActionResult Index(string username)
        {
            if (string.IsNullOrEmpty(username))
                return RedirectToAction("Index", "Home", new { e = "username-empty" });

            Random rnd = new Random();
            var token = CacheHelper.AuthUsers.OrderBy(x => rnd.Next()).FirstOrDefault();
            var info = new Gatherer(token.ConsumerKey, token.ConsumerSecret, token.OAuthToken, token.OAuthTokenSecret);
            var userInfo = info.GetUserInfo(username, "");

            if (userInfo.User == null)
                return RedirectToAction("Index", "Home", new { e = "user_not_found" });

            var userFeed = new Dto.UserFeedResult();
            var populars = _popularUserData.GetAll().OrderBy(x => rnd.Next()).Take(5).ToList();
            if (!userInfo.User.Protected)
                userFeed = info.GetStatusesByUsername(username, "");
            

            var model = new AccountViewModel()
            {
                UserFeeds = userFeed,
                UserInfo = userInfo,
                PopularUsers = populars,
                IsIndex = false,
                IsStats = false,
                IsAds = false,
            };

            var checkBan = CacheHelper.BanUsers.Where(x => x.UserId == userInfo.User.Id).FirstOrDefault();
            if (checkBan != null)
                model.IsAds = false;

            if (userInfo.User.Protected)
            {
                model.IsIndex = false;
                model.IsAds = false;
            }

            if(model.UserFeeds.Statuses.Count > 6)
            {
                model.IsAds = true;
                model.IsIndex = true;
            }

            var userStats = _statData.GetBy(x => x.UserId == userInfo.User.Id);
            if (userStats.Count > 0)
            {
                model.IsStats = true;
                foreach (var item in userStats)
                {
                    model.Followers.Add(item.FollowersCount);
                    model.Friends.Add(item.FriendsCount);
                    model.Tweets.Add(item.TweetCount);
                    model.Date.Add(item.CreateDate.ToString("dd.MM.yyyy"));
                }
            }
            else
            {
                model.IsStats = false;
            }

            return View(model);
        }

        public ActionResult Friends(string username)
        {
            if (string.IsNullOrEmpty(username))
                return RedirectToAction("Index", "Home", new { e = "username-empty" });

            Random rnd = new Random();
            var token = CacheHelper.AuthUsers.OrderBy(x => rnd.Next()).FirstOrDefault();
            var info = new Gatherer(token.ConsumerKey, token.ConsumerSecret, token.OAuthToken, token.OAuthTokenSecret);
            var userInfo = info.GetUserInfo(username, "");

            if (userInfo.User == null)
                return RedirectToAction("Index", "Home", new { e = "user_not_found" });

            var friends = new Dto.UserFriendshipResult();
            var populars = _popularUserData.GetAll().OrderBy(x => rnd.Next()).Take(5).ToList();
            if (!userInfo.User.Protected)
            {
                friends = info.GetFriendsList(username, "");
                
                
            }

            var model = new FriendViewModel()
            {
                UserInfo = userInfo,
                UserFriends = friends,
                PopularUsers = populars,
                IsIndex = true,
                IsAds = true,
            };

            var checkBan = CacheHelper.BanUsers.Where(x => x.UserId == userInfo.User.Id).FirstOrDefault();
            if (checkBan != null)
                model.IsAds = false;

            if (userInfo.User.Protected)
            {
                model.IsAds = false;
                model.IsIndex = false;
            }

            return View(model);
        }

        public ActionResult Followers(string username)
        {
            if (string.IsNullOrEmpty(username))
                return RedirectToAction("Index", "Home", new { e = "username-empty" });

            Random rnd = new Random();
            var token = CacheHelper.AuthUsers.OrderBy(x => rnd.Next()).FirstOrDefault();
            var info = new Gatherer(token.ConsumerKey, token.ConsumerSecret, token.OAuthToken, token.OAuthTokenSecret);
            var userInfo = info.GetUserInfo(username, "");

            if (userInfo.User == null)
            {
                DeteleAsyncDatas(username);
                return RedirectToAction("Index", "Home", new { e = "user_not_found" });
            }

            var followers = new Dto.UserFriendshipResult();
            var populars = _popularUserData.GetAll().OrderBy(x => rnd.Next()).Take(5).ToList();
            if (!userInfo.User.Protected)
            {
                followers = info.GetFollowersList(username, "");
            }

            var model = new FollowersViewModel()
            {
                UserInfo = userInfo,
                UserFollowers = followers,
                PopularUsers = populars,
            };

            var checkBan = CacheHelper.BanUsers.Where(x => x.UserId == userInfo.User.Id).FirstOrDefault();
            if (checkBan != null)
                model.IsAds = false;

            return View(model);
        }

        public JsonResult Ban(string username)
        {
            Random rnd = new Random();
            var token = CacheHelper.AuthUsers.OrderBy(x => rnd.Next()).FirstOrDefault();
            var info = new Gatherer(token.ConsumerKey, token.ConsumerSecret, token.OAuthToken, token.OAuthTokenSecret);
            var userInfo = info.GetUserInfo(username, "");

            if (userInfo.User == null)
                return Json(true, JsonRequestBehavior.AllowGet);

            var check = new BanUserData().GetBy(x => x.UserId == userInfo.User.Id).FirstOrDefault();
            if(check != null)
                return Json(true, JsonRequestBehavior.AllowGet);

            var banUserModel = new BanUser()
            {
                Description = "/ban",
                IsDmca = false,
                UserId = userInfo.User.Id,
            };

            var insert = new BanUserData().Insert(banUserModel);
            if (insert.IsSucceed)
            {
                CacheHelper.BanUsers.Add(banUserModel);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
                
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public async Task<string> GetAsyncStats(Dto.UserFriendshipResult friends, Dto.UserFriendshipResult followers)
        {
            foreach (var item in friends.Users)
            {
                string path = @"c:\temp\" + item.ScreenName + ".txt";
                if (!System.IO.File.Exists(path))
                {
                    using (StreamWriter sw = System.IO.File.CreateText(path))
                    {
                        sw.WriteLine(item.ScreenName);
                    }
                }
            }

            foreach (var item in followers.Users)
            {
                string path = @"c:\temp\" + item.ScreenName + ".txt";
                if (!System.IO.File.Exists(path))
                {
                    using (StreamWriter sw = System.IO.File.CreateText(path))
                    {
                        sw.WriteLine(item.ScreenName);
                    }
                }
            }

            return "error";
        }

        public async Task<string> DeteleAsyncDatas(string username)
        {
            var infos = _statData.GetBy(x => x.ScreenName == username);
            if (infos.Count > 0)
            {
                foreach (var item in infos)
                {
                    _statData.DeleteByKey(item.Id);
                }
            }

            var pop = _popularUserData.GetBy(x => x.Username == username);
            if (pop.Count > 0)
            {
                foreach (var item in pop)
                {
                    _popularUserData.DeleteByKey(item.Id);
                }
            }

            return "error";
        }
    }
}