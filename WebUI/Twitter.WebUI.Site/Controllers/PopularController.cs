﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twitter.Data;
using Twitter.WebUI.Site.Models;

namespace Twitter.WebUI.Site.Controllers
{
    public class PopularController : Controller
    {
        ListData _listData;
        PopularUserData _popularUserData;

        public PopularController()
        {
            _listData = new ListData();
            _popularUserData = new PopularUserData();
        }

        public ActionResult Index(int page = 1)
        {
            var users = _popularUserData.GetByPage(x => x.IsActive, page, 102, "FollowerCount", true);
            var count = _popularUserData.GetCount();

            int total = count / 102;
            total = total + 1;
            var model = new PopularViewModel()
            {
                PopularUsers = users,
                Page = page,
                TotalCount = count,
                TotalPage = total,
            };

            return View(model);
        }

    }
}