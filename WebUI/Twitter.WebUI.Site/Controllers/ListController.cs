﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twitter.Data;
using Twitter.WebUI.Site.Models;

namespace Twitter.WebUI.Site.Controllers
{
    public class ListController : Controller
    {
        ListData _listData;
        PopularUserData _popularUserData;

        public ListController()
        {
            _listData = new ListData();
            _popularUserData = new PopularUserData();
        }

        public ActionResult Index(string slug)
        {
            if (string.IsNullOrEmpty(slug))
                return RedirectToAction("Index", "Home", new { e = "list-name-empty" });

            var item = _listData.GetBy(x => x.Slug == slug).FirstOrDefault();
            if (item == null)
                return RedirectToAction("Index", "Home", new { e = "not-found-list" });


            Random rnd = new Random();

            var randomLists = _listData.GetBy(x => x.IsActive).OrderBy(x => rnd.Next()).Take(3).ToList();

            var users = _popularUserData.GetBy(x => x.ListId == item.Id && x.IsActive).OrderByDescending(x=>x.FollowerCount).ToList();

            var model = new ListViewModel()
            {
                List = item,
                Lists = randomLists,
                PopularUser = users,
            };

            return View(model);
        }

        public ActionResult All()
        {
            var lists = _listData.GetBy(x => x.IsActive);

            var model = new ListViewModel()
            {
                List = null,
                Lists = lists,
                PopularUser = null,
            };

            return View(model);
        }
    }
}