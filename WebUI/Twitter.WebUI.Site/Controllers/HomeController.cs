﻿namespace Twitter.WebUI.Site.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Twitter.Data;
    using Twitter.WebUI.Site.Helpers;
    using Twitter.WebUI.Site.Models;

    public class HomeController : Controller
    {
        ListData _listData;
        PopularUserData _popularUserData;
        HashtagData _hashtagData;

        public HomeController()
        {
            _listData = new ListData();
            _popularUserData = new PopularUserData();
            _hashtagData = new HashtagData();
        }

        public ActionResult Index()
        {
            Random rnd = new Random();
            var list = _listData.GetAll().OrderBy(x => rnd.Next()).Take(8).ToList();
            var users = _popularUserData.GetBy(x => x.IsActive).OrderBy(x => rnd.Next()).Take(15).ToList();
            var hashtags = _hashtagData.GetBy(x => x.IsActive).OrderBy(x => rnd.Next()).Take(9).ToList();


            var model = new HomeViewModel()
            {
                Lists = list,
                PopularUsers = users,
                Hashtags = hashtags,
            };

            return View(model);
        }

        public ActionResult ClearCache()
        {
            CacheHelper.ClearCaches();
            return RedirectToAction("Index", new { q = "reload-cache" });
        }
    }
}