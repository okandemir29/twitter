﻿namespace Twitter.WebUI.Site
{
    using System.Web.Mvc;
    using System.Web.Routing;

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(name: "SearchPage", url: "search/{param}", defaults: new { controller = "Search", action = "Index", param = UrlParameter.Optional });
            routes.MapRoute(name: "AllLists", url: "lists", defaults: new { controller = "List", action = "All" });
            routes.MapRoute(name: "ListDetail", url: "list/{slug}", defaults: new { controller = "List", action = "Index" });
            routes.MapRoute(name: "Discover", url: "dicover-accounts", defaults: new { controller = "Popular", action = "Index" });
            routes.MapRoute(name: "HashtagPage", url: "hashtag/{hashtag}", defaults: new { controller = "Hashtag", action = "Index" });
            routes.MapRoute(name: "UserPage", url: "{username}", defaults: new { controller = "Account", action = "Index" });
            routes.MapRoute(name: "FriendsPage", url: "{username}/friends", defaults: new { controller = "Account", action = "Friends" });
            routes.MapRoute(name: "FollowersPage", url: "{username}/followers", defaults: new { controller = "Account", action = "Followers" });
            routes.MapRoute(name: "BanPage", url: "{username}/ban", defaults: new { controller = "Account", action = "Ban" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
