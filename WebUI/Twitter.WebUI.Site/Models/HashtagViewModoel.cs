﻿namespace Twitter.WebUI.Site.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Twitter.Model;

    public class HashtagViewModoel
    {
        public HashtagViewModoel()
        {
            Hashtags = new Dto.TagFeedResult();
            TrendHashtags = new List<Hashtag>();
        }


        public Dto.TagFeedResult Hashtags { get; set; }
        public List<Hashtag> TrendHashtags { get; set; }
    }
}