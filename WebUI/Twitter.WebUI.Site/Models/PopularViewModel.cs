﻿namespace Twitter.WebUI.Site.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Twitter.Model;

    public class PopularViewModel
    {
        public PopularViewModel()
        {
            PopularUsers = new List<PopularUser>();
        }

        public List<PopularUser> PopularUsers { get; set; }
        public int TotalCount { get; set; }
        public int Page { get; set; }
        public int TotalPage { get; set; }
    }
}