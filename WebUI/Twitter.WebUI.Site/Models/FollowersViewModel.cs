﻿namespace Twitter.WebUI.Site.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Twitter.Model;

    public class FollowersViewModel
    {
        public FollowersViewModel()
        {
            PopularUsers = new List<PopularUser>();
        }

        public Dto.UserInfoResult UserInfo { get; set; }
        public Dto.UserFriendshipResult UserFollowers { get; set; }
        public List<PopularUser> PopularUsers { get; set; }

        public bool IsIndex { get; set; }
        public bool IsAds { get; set; }
    }
}