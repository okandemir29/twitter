﻿namespace Twitter.WebUI.Site.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Twitter.Model;

    public class ListViewModel
    {
        public ListViewModel()
        {
            Lists = new List<Model.List>();
            PopularUser = new List<PopularUser>();
        }

        public List<Model.List> Lists { get; set; }
        public Model.List List { get; set; }
        public List<PopularUser> PopularUser { get; set; }
    }
}