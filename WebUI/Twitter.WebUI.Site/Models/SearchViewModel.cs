﻿namespace Twitter.WebUI.Site.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public class SearchViewModel
    {
        public SearchViewModel()
        {
            Users = new Dto.UserSuggestResult();
            Populars = new List<Model.PopularUser>();
            Param = "";
        }

        public Dto.UserSuggestResult Users { get; set; }
        public List<Model.PopularUser> Populars { get; set; }
        public string Param { get; set; }
    }
}