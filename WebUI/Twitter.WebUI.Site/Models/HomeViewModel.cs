﻿namespace Twitter.WebUI.Site.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public class HomeViewModel
    {
        public HomeViewModel()
        {
            Lists = new List<Model.List>();
            PopularUsers = new List<Model.PopularUser>();
            Hashtags = new List<Model.Hashtag>();
        }

        public List<Model.List> Lists { get; set; }
        public List<Model.PopularUser> PopularUsers { get; set; }
        public List<Model.Hashtag> Hashtags { get; set; }
    }
}