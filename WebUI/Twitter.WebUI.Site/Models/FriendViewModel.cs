﻿namespace Twitter.WebUI.Site.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Twitter.Model;

    public class FriendViewModel
    {
        public FriendViewModel()
        {
            PopularUsers = new List<PopularUser>();
        }

        public Dto.UserInfoResult UserInfo { get; set; }
        public Dto.UserFriendshipResult UserFriends { get; set; }
        public List<PopularUser> PopularUsers { get; set; }

        public bool IsIndex { get; set; }
        public bool IsAds { get; set; }

        public List<int> Tweets { get; set; }
    }
}