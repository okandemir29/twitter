﻿namespace Twitter.WebUI.Site.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Twitter.Model;

    public class AccountViewModel
    {
        public AccountViewModel()
        {
            Followers = new List<int>();
            Date = new List<string>();
            Friends = new List<int>();
            Tweets = new List<int>();
            PopularUsers = new List<PopularUser>();
        }

        public Dto.UserInfoResult UserInfo { get; set; }
        public Dto.UserFeedResult UserFeeds { get; set; }
        public List<PopularUser> PopularUsers { get; set; }
        public bool IsStats { get; set; }
        public bool IsAds { get; set; }
        public bool IsIndex { get; set; }

        public List<int> Followers { get; set; }
        public List<string> Date { get; set; }
        public List<int> Friends { get; set; }
        public List<int> Tweets { get; set; }
    }
}