﻿namespace Twitter.Model
{
    public class Banword : Core.ModelBase
    {
        public string Value { get; set; }
        public string Description { get; set; }
        public int Type { get; set; }
    }
}
