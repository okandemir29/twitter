﻿namespace Twitter.Model
{
    using System;

    public class Stat : Core.ModelBase
    {
        public string UserId { get; set; }
        public string ScreenName { get; set; }
        public int TweetCount { get; set; }
        public int FriendsCount { get; set; }
        public int FollowersCount { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
