﻿namespace Twitter.Model
{
    public class OperationValue : Core.ModelBase
    {
        public int LanguageId { get; set; }
        public int OperationLabelId { get; set; }
        public string Value { get; set; }
    }
}
