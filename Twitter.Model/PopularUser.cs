﻿namespace Twitter.Model
{
    public class PopularUser : Core.ModelBase
    {
        public string Username { get; set; }
        public string UserId { get; set; }
        public string Fullname { get; set; }
        public string ImagePath { get; set; }
        public int TweetCount { get; set; }
        public int FollowerCount { get; set; }
        public int FriendCount { get; set; }
        public int ListId { get; set; }
        public bool IsActive { get; set; }
    }
}
