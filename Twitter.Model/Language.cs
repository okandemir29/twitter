﻿namespace Twitter.Model
{
    public class Language : Core.ModelBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Flag { get; set; }
    }
}
