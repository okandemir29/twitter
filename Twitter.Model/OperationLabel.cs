﻿namespace Twitter.Model
{
    public class OperationLabel : Core.ModelBase
    {
        public string Key { get; set; }
        public string Description { get; set; }
    }
}
