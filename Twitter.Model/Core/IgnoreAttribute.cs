﻿using System;

namespace Twitter.Model.Core
{
    public class IgnoreAttribute : Attribute
    {
        public string SomeProperty { get; set; }
    }
}
