﻿namespace Twitter.Model
{
    public class BanUser : Core.ModelBase
    {
        public string UserId { get; set; }
        public bool IsDmca { get; set; }
        public string Description { get; set; }
    }
}
