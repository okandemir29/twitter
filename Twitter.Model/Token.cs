﻿namespace Twitter.Model
{
    public class Token : Core.ModelBase
    {
        public string AccessSecret { get; set; }
        public string AccessToken { get; set; }
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
        public bool IsClosedByService { get; set; }
        public int SiteId { get; set; }
    }
}
