﻿namespace Twitter.Model
{
    public class Hashtag : Core.ModelBase
    {
        public string Name { get; set; }
        public string Icon { get; set; }
        public bool IsActive { get; set; }
    }
}
