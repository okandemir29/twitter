﻿namespace Twitter.Model
{
    public class AuthUser : Core.ModelBase
    {
        public string OAuthToken { get; set; }
        public string OAuthTokenSecret { get; set; }
        public string ScreenName { get; set; }
        public string UserId { get; set; }
        public bool IsActive { get; set; }
        public int SiteId { get; set; }
        public int TokenId { get; set; }
    }
}
