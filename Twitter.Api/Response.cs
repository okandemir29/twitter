﻿namespace Twitter.Api
{
    public class Response<T>
    {
        public T Result { get; set; }
        public bool IsSucceed { get; set; }
        public string Message { get; set; }
    }
}
