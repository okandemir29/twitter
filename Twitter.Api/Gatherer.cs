﻿namespace Twitter.Api
{
    using Dto;
    using Entities;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Security.Cryptography;
    using System.Text;

    public class Gatherer
    {
        private static string _baseApiUrl = "https://api.twitter.com/1.1";

        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
        public string AccessToken { get; set; }
        public string AccessTokenSecret { get; set; }

        public Gatherer(string consumerKey, string consumerSecret, string accessToken, string accessTokenSecret)
        {
            ConsumerKey = consumerKey;
            ConsumerSecret = consumerSecret;
            AccessToken = accessToken;
            AccessTokenSecret = accessTokenSecret;
        }

        /// <summary>
        /// GET statuses/mentions_timeline
        /// https://api.twitter.com/1.1/statuses/mentions_timeline.json
        /// Returns the 20 most recent mentions (tweets containing a users’s @screen_name) for the authenticating user.
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public Response<List<Status>> GetStatusesMentions(int count = 20)
        {
            Response<List<Status>> response;

            var prefix = string.Format("count={0}&", count);
            var postfix = "";
            var querystring = string.Format("?count={0}", count);

            var strJson = Get(prefix, postfix, "/statuses/mentions_timeline.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<Status>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<Status>()
                };
            }
            else {
                var jArr = JArray.Parse(strJson);
                var list = Status.ParseMany(jArr);

                response = new Response<List<Status>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }

            return response;
        }

        /// <summary>
        /// It is enough to send one of screen_name or userd_id
        /// GET statuses/user_timeline
        /// https://api.twitter.com/1.1/statuses/user_timeline.json
        /// Returns a collection of the most recent Tweets posted by the user indicated by the screen_name or user_id parameters.
        /// </summary>
        /// <param name="screenName"></param>
        /// <param name="userId"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public UserFeedResult GetStatusesByUsername(string screenName, string userId,int count = 50,string maxId = "")
        {
            UserFeedResult response;

            var prefix = string.IsNullOrEmpty(maxId)
                ? $"count={count}&"
                : string.Format("count={0}&max_id={1}&", count, maxId);
            var postfix = (!string.IsNullOrEmpty(screenName))
                ? $"&screen_name={Uri.EscapeDataString(screenName)}&tweet_mode=extended"
                : $"&user_id={Uri.EscapeDataString(userId)}&tweet_mode=extended";

            var queryString = (!string.IsNullOrEmpty(screenName))
                ? string.IsNullOrEmpty(maxId)
                    ? $"?screen_name={Uri.EscapeDataString(screenName)}&count={count}&tweet_mode=extended"
                    : $"?screen_name={Uri.EscapeDataString(screenName)}&count={count}&max_id={maxId}&tweet_mode=extended"
                : string.IsNullOrEmpty(maxId)
                    ? $"?user_id={Uri.EscapeDataString(userId)}&count={count}&tweet_mode=extended"
                    : $"?user_id={Uri.EscapeDataString(userId)}&count={count}&max_id={maxId}&tweet_mode=extended";

            var strJson = "";

            strJson = (!string.IsNullOrEmpty(screenName))
                 ? Get(prefix, postfix, "/statuses/user_timeline.json", queryString)
                 : Get(prefix, postfix, "/statuses/user_timeline.json", queryString);
            
            if (string.IsNullOrEmpty(strJson))
            {
                response = new UserFeedResult()
                {
                    IsSucceed = false,
                    Message = "",
                    LastId = "",
                    Statuses = new List<Dto.Base.Status>(),
                    User = new Dto.Base.User()
                    {
                        ScreenName = screenName
                    }
                };
            }
            else
            {
                var jArr = JArray.Parse(strJson);
                var list = Status.ParseMany(jArr);

                response = new UserFeedResult()
                {
                    IsSucceed = true,
                    Message = "",
                    LastId = "",
                    Statuses = list.Select(x => x.ParseToDto()).ToList(),
                    User = new Dto.Base.User()
                    {
                        ScreenName = screenName
                    }
                };
            }

            return response;
        }

        /// <summary>
        /// GET statuses/home_timeline
        /// https://api.twitter.com/1.1/statuses/home_timeline.json
        /// Returns a collection of the most recent Tweets and retweets posted by the authenticating user and the users they follow. 
        /// The home timeline is central to how most users interact with the Twitter service.
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public Response<List<Status>> GetStatusesHomeTimeline(int count = 20)
        {
            Response<List<Status>> response;

            var prefix = string.Format("count={0}&", count);
            var postfix = "";
            var querystring = string.Format("?count={0}", count);

            var strJson = Get(prefix, postfix, "/statuses/home_timeline.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<Status>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<Status>()
                };
            }
            else
            {
                var jArr = JArray.Parse(strJson);
                var list = Status.ParseMany(jArr);

                response = new Response<List<Status>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }

            return response;
        }

        /// <summary>
        /// GET statuses/retweets_of_me
        /// https://api.twitter.com/1.1/statuses/retweets_of_me.json
        /// Returns the most recent tweets authored by the authenticating user that have been retweeted by others.
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public Response<List<Status>> GetStatusesRetweetsOfMe(int count = 20)
        {
            Response<List<Status>> response;

            var prefix = string.Format("count={0}&", count);
            var postfix = "";
            var querystring = string.Format("?count={0}", count);

            var strJson = Get(prefix, postfix, "/statuses/retweets_of_me.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<Status>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<Status>()
                };
            }
            else
            {
                var jArr = JArray.Parse(strJson);
                var list = Status.ParseMany(jArr);

                response = new Response<List<Status>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }

            return response;
        }

        /// <summary>
        /// GET statuses/retweets/:id
        /// https://api.twitter.com/1.1/statuses/retweets/:id.json
        /// Returns a collection of the 100 most recent retweets of the tweet specified by the id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public Response<List<Status>> GetStatusesRetweets(string id, int count = 20)
        {
            Response<List<Status>> response;

            var prefix = string.Format("count={0}&", count);
            var postfix = "";
            var querystring = string.Format("?count={0}", count);

            var strJson = Get(prefix, postfix, "/statuses/retweets/" + id + ".json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<Status>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<Status>()
                };
            }
            else
            {
                var jArr = JArray.Parse(strJson);
                var list = Status.ParseMany(jArr);

                response = new Response<List<Status>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }

            return response;
        }

        public StatusResult GetRetweetById(string statusId)
        {
            StatusResult response;
            var prefix = "";
            var postfix = "";
            var querystring = "";

            var strJson = Get(prefix, postfix, "/statuses/show/" + statusId + ".json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new StatusResult()
                {
                    IsSucceed = false,
                    Message = "",
                    Status = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var item = Status.Parse(jObj);

                response = new StatusResult()
                {
                    IsSucceed = true,
                    Message = "",
                    Status = item.ParseToDto()
                };
            }

            return response;
        }

        /// <summary>
        /// GET statuses/show/:id
        /// https://api.twitter.com/1.1/statuses/show.json
        /// Returns a single Tweet, specified by the id parameter. The Tweet’s author will also be embedded within the tweet.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public StatusResult GetStatusById(string statusId)
        {
            StatusResult response;
            var prefix = "include_entities=true&";
            var postfix = "";
            var querystring = "?include_entities=true";

            var strJson = Get(prefix, postfix, "/statuses/show/" + statusId + ".json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new StatusResult()
                {
                    IsSucceed = false,
                    Message = "",
                    Status = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var item = Status.Parse(jObj);

                response = new StatusResult()
                {
                    IsSucceed = true,
                    Message = "",
                    Status = item.ParseToDto()
                };
            }

            return response;
        }

        /// <summary>
        /// POST statuses/destroy/:id
        /// https://api.twitter.com/1.1/statuses/destroy/:id.json
        /// Destroys the status specified by the required ID parameter.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response<Status> PostStatusesDestroy(string id)
        {
            Response<Status> response;
            var prefix = "";
            var postfix = "";
            var querystring = "";

            var strJson = Post(prefix, postfix, "/statuses/destroy/" + id + ".json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<Status>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var item = Status.Parse(jObj);

                response = new Response<Status>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = item
                };
            }

            return response;
        }

        /// <summary>
        /// POST statuses/update
        /// https://api.twitter.com/1.1/statuses/update.json
        /// Updates the authenticating user’s current status, also known as Tweeting.
        /// <param name="text"></param>
        /// </summary>
        public Response<Status> PostStatusesUpdate(string text)
        {
            Response<Status> response;
            var prefix = "";
            var postfix = "&status=" + Uri.EscapeDataString(text);
            var querystring = "status=" + Uri.EscapeDataString(text);

            var strJson = Post(prefix, postfix, "/statuses/update.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<Status>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var item = Status.Parse(jObj);

                response = new Response<Status>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = item
                };
            }

            return response;
        }

        /// <summary>
        /// POST statuses/retweet/:id
        /// https://api.twitter.com/1.1/statuses/retweet/:id.json
        /// Retweets a tweet. Returns the original tweet with retweet details embedded.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response<Status> PostStatusesRetweet(string id)
        {
            Response<Status> response;
            var prefix = "";
            var postfix = "";
            var querystring = "";

            var strJson = Post(prefix, postfix, "/statuses/retweet/" + id + ".json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<Status>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var item = Status.Parse(jObj);

                response = new Response<Status>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = item
                };
            }

            return response;
        }

        /// <summary>
        /// POST statuses/retweet/:id
        /// https://api.twitter.com/1.1/statuses/unretweet/:id.json
        /// Untweets a retweeted status. Returns the original Tweet with retweet details embedded.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response<Status> PostStatusesUnretweet(string id)
        {
            Response<Status> response;
            var prefix = "";
            var postfix = "";
            var querystring = "";

            var strJson = Post(prefix, postfix, "/statuses/unretweet/" + id + ".json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<Status>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var item = Status.Parse(jObj);

                response = new Response<Status>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = item
                };
            }

            return response;
        }

        /// <summary>
        /// GET statuses/retweeters/ids
        /// https://api.twitter.com/1.1/statuses/retweeters/ids.json
        /// Returns a collection of up to 100 user IDs belonging to users who have retweeted the tweet specified by the id parameter.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response<List<string>> GetStatusesRetweetersIds(string id)
        {
            Response<List<string>> response;
            var prefix = string.Format("id={0}&", id);
            var postfix = "&stringify_ids=true";
            var querystring = string.Format("?id={0}&stringify_ids=true", id);

            var strJson = Get(prefix, postfix, "/statuses/retweeters/ids.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<string>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<string>()
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var jArr = JArray.Parse(jObj["ids"].ToString());

                var list = new List<string>();
                foreach (var jToken in jArr)
                    list.Add(jToken.ToString());

                response = new Response<List<string>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }

            return response;
        }

        /// <summary>
        /// GET statuses/lookup
        /// https://api.twitter.com/1.1/statuses/lookup.json
        /// Returns fully-hydrated tweet objects for up to 100 tweets per request, as specified by comma-separated values passed to the id parameter.
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public Response<List<Status>> GetStatusesLookup(string ids)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GET direct_messages/sent
        /// https://api.twitter.com/1.1/direct_messages/sent.json
        /// Returns the 20 most recent direct messages sent by the authenticating user. Includes detailed information about the sender and recipient user
        /// </summary>
        /// <returns></returns>
        public Response<List<DirectMessage>> GetDirectMessagesSent(int count = 20)
        {
            Response<List<DirectMessage>> response;
            var prefix = string.Format("count={0}&", count);
            var postfix = "";
            var querystring = string.Format("?count={0}", count);

            var strJson = Get(prefix, postfix, "/direct_messages/sent.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<DirectMessage>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<DirectMessage>()
                };
            }
            else
            {
                var jArr = JArray.Parse(strJson);
                var list = DirectMessage.ParseMany(jArr);

                response = new Response<List<DirectMessage>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }

            return response;
        }

        /// <summary>
        /// GET statuses/show/:id
        /// https://api.twitter.com/1.1/statuses/show.json
        /// Returns a single Tweet, specified by the id parameter. The Tweet’s author will also be embedded within the tweet.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response<DirectMessage> GetDirectMessagesShow(string id)
        {
            Response<DirectMessage> response;
            var prefix = "";
            var postfix = "";
            var querystring = "";

            var strJson = Get(prefix, postfix, "/direct_messages/show/" + id + ".json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<DirectMessage>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var item = DirectMessage.Parse(jObj);

                response = new Response<DirectMessage>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = item
                };
            }

            return response;
        }

        /// <summary>
        /// GET search/tweets
        /// https://api.twitter.com/1.1/search/tweets.json
        /// Returns a collection of relevant Tweets matching a specified query.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public TagFeedResult GetStatusesByTag(string tag, int count = 50, string maxId = "")
        {
            TagFeedResult response;
            var prefix = string.IsNullOrEmpty(maxId)
                ? $"count={count}&"
                : $"count={count}&max_id={maxId}&";

            var postfix = string.Format("&q={0}", Uri.EscapeDataString(tag));
            var querystring = string.IsNullOrEmpty(maxId)
                ? $"?q={Uri.EscapeDataString(tag)}&count={count}"
                : $"?q={Uri.EscapeDataString(tag)}&count={count}&max_id={maxId}";

            var strJson = Get(prefix, postfix, "/search/tweets.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new TagFeedResult()
                {
                    IsSucceed = false,
                    Message = "",
                    Statuses = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var jArr = JArray.Parse(jObj["statuses"].ToString());
                var list = Status.ParseMany(jArr);

                response = new TagFeedResult()
                {
                    IsSucceed = true,
                    Message = "",
                    Statuses = list.Select(x => x.ParseToDto()).ToList(),
                    Tag = tag,
                    LastId = ""
                };
            }

            return response;
        }

        /// <summary>
        /// GET direct_messages
        /// https://api.twitter.com/1.1/direct_messages.json
        /// Returns the 20 most recent direct messages sent to the authenticating user. Includes detailed information about the sender and recipient user.
        /// </summary>
        /// <returns></returns>
        public Response<List<DirectMessage>> GetDirectMessages(int count = 20)
        {
            Response<List<DirectMessage>> response;
            var prefix = string.Format("count={0}&", count);
            var postfix = "";
            var querystring = string.Format("?count={0}", count);

            var strJson = Get(prefix, postfix, "/direct_messages.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<DirectMessage>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<DirectMessage>()
                };
            }
            else
            {
                var jArr = JArray.Parse(strJson);
                var list = DirectMessage.ParseMany(jArr);

                response = new Response<List<DirectMessage>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }

            return response;
        }

        /// <summary>
        /// POST direct_messages/destroy
        /// https://api.twitter.com/1.1/direct_messages/destroy.json
        /// Destroys the direct message specified in the required ID parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response<DirectMessage> PostDirectMessagesDestroy(string id)
        {
            Response<DirectMessage> response;
            var prefix = "";
            var postfix = "";
            var querystring = "";

            var strJson = Post(prefix, postfix, "/direct_messages/destroy/" + id + ".json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<DirectMessage>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = null
                };
            }
            else {
                var jObj = JObject.Parse(strJson);
                var item = DirectMessage.Parse(jObj);

                response = new Response<DirectMessage>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = item
                };
            }

            return response;
        }

        /// <summary>
        /// POST direct_messages/new
        /// https://api.twitter.com/1.1/direct_messages/new.json
        /// Sends a new direct message to the specified user from the authenticating user. 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="screenName"></param>
        /// <returns></returns>
        public Response<DirectMessage> PostDirectMessageNew(string text, string screenName)
        {
            Response<DirectMessage> response;

            var prefix = "";
            var postfix = string.Format("&screen_name={0}&text={1}",screenName, Uri.EscapeDataString(text));
            var postBody = string.Format("screen_name={0}&text={1}", screenName, Uri.EscapeDataString(text));

            var strJson = Post(prefix, postfix, "/direct_messages/new.json", postBody);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<DirectMessage>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var item = DirectMessage.Parse(jObj);

                response = new Response<DirectMessage>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = item
                };
            }

            return response;
        }

        /// <summary>
        /// GET friendships/no_retweets/ids
        /// https://api.twitter.com/1.1/friendships/no_retweets/ids.json
        /// Returns a collection of user_ids that the currently authenticated user does not want to receive retweets from.
        /// </summary>
        /// <returns></returns>
        public Response<List<string>> GetFriendshipsNoRetweetsIds()
        {
            Response<List<string>> response;
            var prefix = "";
            var postfix = "&stringify_ids=true";
            var querystring = string.Format("?stringify_ids=true");

            var strJson = Get(prefix, postfix, "/friendships/no_retweets/ids.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<string>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = null
                };
            }
            else
            {
                var jArr = JArray.Parse(strJson);

                var list = new List<string>();
                foreach (var jToken in jArr)
                    list.Add(jToken.ToString());

                response = new Response<List<string>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }

            return response;
        }

        /// <summary>
        /// It is enough to send one of screen_name or userd_id
        /// GET friends/ids
        /// https://api.twitter.com/1.1/friends/ids.json
        /// Returns a cursored collection of user IDs for every user the specified user is following (otherwise known as their “friends”).
        /// </summary>
        /// <param name="screenName"></param>
        /// <param name="userId"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public Response<List<string>> GetFriendsIds(string screenName, string userId, int count = 20)
        {
            Response<List<string>> response;

            var prefix = string.Format("count={0}&", count);
            var strJson = "";

            strJson = (!string.IsNullOrEmpty(screenName))
                 ? Get(prefix, "&screen_name=" + Uri.EscapeDataString(screenName), "/friends/ids.json", "?screen_name=" + Uri.EscapeDataString(screenName) + "&count=" + count)
                 : Get(prefix, "&user_id=" + Uri.EscapeDataString(userId), "/friends/ids.json", "?user_id=" + Uri.EscapeDataString(userId) + "&count=" + count);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<string>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<string>()
                };
            }
            else
            {
                var JObj = JObject.Parse(strJson);
                var jArr = JArray.Parse(JObj["ids"].ToString());
                var list = new List<string>();
                foreach (var jToken in jArr)
                    list.Add(jToken.ToString());

                response = new Response<List<string>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }

            return response;
        }

        /// <summary>
        /// It is enough to send one of screen_name or userd_id
        /// GET followers/ids
        /// https://api.twitter.com/1.1/followers/ids.json
        /// Returns a cursored collection of user IDs for every user the specified user is following (otherwise known as their “friends”).
        /// </summary>
        /// <param name="screenName"></param>
        /// <param name="userId"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public Response<List<string>> GetFollowersIds(string screenName, string userId, int count = 20)
        {
            Response<List<string>> response;
            var prefix = string.Format("count={0}&", count);
            var strJson = "";

            strJson = (!string.IsNullOrEmpty(screenName))
                 ? Get(prefix, "&screen_name=" + Uri.EscapeDataString(screenName), "/followers/ids.json", "?screen_name=" + Uri.EscapeDataString(screenName) + "&count=" + count)
                 : Get(prefix, "&user_id=" + Uri.EscapeDataString(userId), "/followers/ids.json", "?user_id=" + Uri.EscapeDataString(userId) + "&count=" + count);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<string>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<string>()
                };
            }
            else
            {
                var JObj = JObject.Parse(strJson);
                var jArr = JArray.Parse(JObj["ids"].ToString());
                var list = new List<string>();
                foreach (var jToken in jArr)
                    list.Add(jToken.ToString());

                response = new Response<List<string>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }

            return response;
        }

        /// <summary>
        /// GET friendships/incoming
        /// https://api.twitter.com/1.1/friendships/incoming.json
        /// Returns a collection of user_ids that the currently authenticated user does not want to receive retweets from.
        /// </summary>
        /// <returns></returns>
        public Response<List<string>> GetFriendshipsIncoming()
        {
            Response<List<string>> response;
            var prefix = "";
            var postfix = "";
            var querystring = "";

            var strJson = Get(prefix, postfix, "/friendships/incoming.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<string>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<string>()
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var jArr = JArray.Parse(jObj["ids"].ToString());

                var list = new List<string>();
                foreach (var jToken in jArr)
                    list.Add(jToken.ToString());

                response = new Response<List<string>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }

            return response;
        }

        /// <summary>
        /// GET friendships/outgoing
        /// https://api.twitter.com/1.1/friendships/outgoing.json
        /// Returns a collection of numeric IDs for every protected user for whom the authenticating user has a pending follow request.
        /// </summary>
        /// <returns></returns>
        public Response<List<string>> GetFriendshipsOutgoing()
        {
            Response<List<string>> response;

            var prefix = "";
            var postfix = "";
            var querystring = "";

            var strJson = Get(prefix, postfix, "/friendships/outgoing.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<string>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<string>()
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var jArr = JArray.Parse(jObj["ids"].ToString());

                var list = new List<string>();
                foreach (var jToken in jArr)
                    list.Add(jToken.ToString());

                response = new Response<List<string>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }

            return response;
        }

        /// <summary>
        /// POST friendships/create
        /// https://api.twitter.com/1.1/friendships/create.json
        /// Allows the authenticating users to follow the user specified in the ID parameter.
        /// <param name="screenName"></param>
        /// <param name="userId"></param>
        /// </summary>
        public Response<User> PostFriendshipsCreate(string screenName, string userId)
        {
            Response<User> response;

            var prefix = "";
            var postfix = !string.IsNullOrEmpty(screenName)
                ? string.Format("&screen_name={0}", Uri.EscapeDataString(screenName))
                : string.Format("&user_id={0}", Uri.EscapeDataString(userId));
            var postbody = !string.IsNullOrEmpty(screenName)
                ? string.Format("screen_name={0}", Uri.EscapeDataString(screenName))
                : string.Format("user_id={0}", Uri.EscapeDataString(userId));

            var strJson = Post(prefix, postfix, "/friendships/create.json", postbody);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<User>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var user = User.Parse(jObj);

                response = new Response<User>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = user
                };
            }

            return response;
        }

        /// <summary>
        /// POST friendships/destroy
        /// https://api.twitter.com/1.1/friendships/destroy.json
        /// Allows the authenticating user to unfollow the user specified in the ID parameter.
        /// </summary>
        /// <param name="screenName"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Response<User> PostFriendshipsDestroy(string screenName, string userId)
        {
            Response<User> response;
            var prefix = "";
            var postfix = !string.IsNullOrEmpty(screenName)
                ? string.Format("&screen_name={0}", Uri.EscapeDataString(screenName))
                : string.Format("&user_id={0}", Uri.EscapeDataString(userId));
            var postbody = !string.IsNullOrEmpty(screenName)
                ? string.Format("screen_name={0}", Uri.EscapeDataString(screenName))
                : string.Format("user_id={0}", Uri.EscapeDataString(userId));

            var strJson = Post(prefix, postfix, "/friendships/destroy.json", postbody);

            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<User>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var user = User.Parse(jObj);

                response = new Response<User>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = user
                };
            }

            return response;
        }

        /// <summary>
        /// GET friendships/show
        /// https://api.twitter.com/1.1/friendships/show.json
        /// Returns detailed information about the relationship between two arbitrary users.
        /// </summary>
        /// <param name="sourceScreenName"></param>
        /// <param name="targetScreenName"></param>
        /// <returns></returns>
        public Response<string> GetFriendshipsShow(string sourceScreenName, string targetScreenName)
        {
            Response<string> response;

            var prefix = "";
            var postfix = string.Format("&source_screen_name={0}&target_screen_name={1}", Uri.EscapeDataString(sourceScreenName), Uri.EscapeDataString(targetScreenName));
            var querystring = string.Format("?source_screen_name={0}&target_screen_name={1}", Uri.EscapeDataString(sourceScreenName), Uri.EscapeDataString(targetScreenName));
            var strJson = Get(prefix, postfix, "/friendships/show.json", querystring);

            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<string>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = ""
                };
            }
            else
            {
                response = new Response<string>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = strJson
                };
            }

            return response;
        }

        /// <summary>
        /// GET friends/list
        /// https://api.twitter.com/1.1/friends/list.json
        /// Returns a cursored collection of user objects for every user the specified user is following (otherwise known as their “friends”).
        /// </summary>
        /// <param name="screenName"></param>
        /// <param name="userId"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public UserFriendshipResult GetFriendsList(string screenName, string userId, int count = 75)
        {
            UserFriendshipResult response;
            var prefix = string.Format("count={0}&", count);
            var postfix = !string.IsNullOrEmpty(screenName)
                ? string.Format("&screen_name={0}", Uri.EscapeDataString(screenName))
                : string.Format("&user_id={0}", Uri.EscapeDataString(userId));
            var querystring = !string.IsNullOrEmpty(screenName)
                ? string.Format("?screen_name={0}&count={1}", Uri.EscapeDataString(screenName), count)
                : string.Format("?user_id={0}&count={1}", Uri.EscapeDataString(userId), count);

            var strJson = Get(prefix, postfix, "/friends/list.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new UserFriendshipResult()
                {
                    IsSucceed = false,
                    Message = "",
                    User = new Dto.Base.User()
                    {
                        ScreenName = screenName
                    },
                    Users = new List<Dto.Base.UserSimple>()
                };
            }
            else
            {
                JObject jObj = JObject.Parse(strJson);
                JArray jArr = JArray.Parse(jObj["users"].ToString());

                var list = UserSimple.ParseMany(jArr, count);

                response = new UserFriendshipResult()
                {
                    IsSucceed = true,
                    Message = "",
                    User = new Dto.Base.User() { ScreenName = screenName },
                    Users = list.Select(x => x.ParseToDto()).ToList()
                };
            }

            return response;
        }

        /// <summary>
        /// GET followers/list
        /// https://api.twitter.com/1.1/followers/list.json
        /// Returns a cursored collection of user objects for users following the specified user.
        /// </summary>
        /// <param name="screenName"></param>
        /// <param name="userId"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public UserFriendshipResult GetFollowersList(string screenName, string userId, int count = 75)
        {
            UserFriendshipResult response;
            var prefix = string.Format("count={0}&", count);
            var postfix = !string.IsNullOrEmpty(screenName)
                ? string.Format("&screen_name={0}", Uri.EscapeDataString(screenName))
                : string.Format("&user_id={0}", Uri.EscapeDataString(userId));
            var querystring = !string.IsNullOrEmpty(screenName)
                ? string.Format("?screen_name={0}&count={1}", Uri.EscapeDataString(screenName), count)
                : string.Format("?user_id={0}&count={1}", Uri.EscapeDataString(userId), count);

            var strJson = Get(prefix, postfix, "/followers/list.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new UserFriendshipResult()
                {
                    IsSucceed = false,
                    Message = "",
                    User = new Dto.Base.User()
                    {
                        ScreenName = screenName
                    },
                    Users = new List<Dto.Base.UserSimple>()
                };
            }
            else
            {
                JObject jObj = JObject.Parse(strJson);
                JArray jArr = JArray.Parse(jObj["users"].ToString());

                var list = UserSimple.ParseMany(jArr, count);

                response = new UserFriendshipResult()
                {
                    IsSucceed = true,
                    Message = "",
                    User = new Dto.Base.User()
                    {
                        ScreenName = screenName
                    },
                    Users = list.Select(x => x.ParseToDto()).ToList()
                };
            }

            return response;
        }

        /// <summary>
        /// GET account/settings
        /// https://api.twitter.com/1.1/account/settings.json
        /// Returns settings (including current trend, geo and sleep time information) for the authenticating user.
        /// </summary>
        /// <returns></returns>
        public Response<AccountSetting> GetAccountSettings()
        {
            Response<AccountSetting> response;

            var prefix = "";
            var postfix = "";
            var querystring = "";

            var strJson = Get(prefix, postfix, "/account/settings.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<AccountSetting>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var sett = AccountSetting.Parse(jObj);

                response = new Response<AccountSetting>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = sett
                };
            }

            return response;
        }

        /// <summary>
        /// POST account/update_profile
        /// https://api.twitter.com/1.1/account/update_profile.json
        /// Sets some values that users are able to set under the “Account” tab of their settings page.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="url"></param>
        /// <param name="location"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public Response<User> PostAccountUpdateProfile(string name, string url, string location, string description)
        {
            Response<User> response;

            url = url.Replace("http://", "");

            var prefix = string.Format("description={3}&location={2}&name={0}&", Uri.EscapeDataString(name), Uri.EscapeDataString(url)
                , Uri.EscapeDataString(location), Uri.EscapeDataString(description));
            var postfix = string.Format("&url={0}", url);
            var postbody = string.Format("description={3}&location={2}&name={0}&url={1}", Uri.EscapeDataString(name), Uri.EscapeDataString(url)
                , Uri.EscapeDataString(location), Uri.EscapeDataString(description));

            var strJson = Post(prefix, postfix, "/account/update_profile.json", postbody);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<User>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var user = User.Parse(jObj);

                response = new Response<User>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = user
                };
            }

            return response;
        }

        /// <summary>
        /// POST account/update_profile_image
        /// https://api.twitter.com/1.1/account/update_profile_image.json
        /// Updates the authenticating user’s profile image. Note that this method expects raw multipart data, not a URL to an image.
        /// </summary>
        /// <param name="imagePath"></param>
        /// <returns></returns>
        public Response<User> PostAccountUpdateProfileImage(string imagePath)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GET blocks/list
        /// https://api.twitter.com/1.1/blocks/list.json
        /// Returns a collection of user objects that the authenticating user is blocking.
        /// </summary>
        /// <returns></returns>
        public Response<List<User>> GetBlocksList()
        {
            Response<List<User>> response;
            var prefix = "";
            var postfix = "";
            var querystring = "";

            var strJson = Get(prefix, postfix, "/blocks/list.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<User>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<User>()
                };
            }
            else
            {
                JObject jObj = JObject.Parse(strJson);
                JArray jArr = JArray.Parse(jObj["users"].ToString());

                var list = User.ParseMany(jArr);

                response = new Response<List<User>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }

            return response;
        }

        /// <summary>
        /// GET blocks/ids
        /// https://api.twitter.com/1.1/blocks/ids.json
        /// Returns an array of numeric user ids the authenticating user is blocking.
        /// </summary>
        /// <returns></returns>
        public Response<List<string>> GetBlocksIds()
        {
            Response<List<string>> response;
            var prefix = "";
            var postfix = "";
            var querystring = "";

            var strJson = Get(prefix, postfix, "/blocks/ids.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<string>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<string>()
                };
            }
            else
            {
                var JObj = JObject.Parse(strJson);
                var jArr = JArray.Parse(JObj["ids"].ToString());
                var list = new List<string>();
                foreach (var jToken in jArr)
                    list.Add(jToken.ToString());

                response = new Response<List<string>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }

            return response;
        }

        /// <summary>
        /// POST blocks/create
        /// https://api.twitter.com/1.1/blocks/create.json
        /// Blocks the specified user from following the authenticating user.
        /// <param name="screenName"></param>
        /// <param name="userId"></param>
        /// </summary>
        public Response<User> PostBlocksCreate(string screenName, string userId)
        {
            Response<User> response;

            var prefix = "";
            var postfix = !string.IsNullOrEmpty(screenName)
                ? string.Format("&screen_name={0}", Uri.EscapeDataString(screenName))
                : string.Format("&user_id={0}", Uri.EscapeDataString(userId));
            var postbody = !string.IsNullOrEmpty(screenName)
                ? string.Format("screen_name={0}", Uri.EscapeDataString(screenName))
                : string.Format("user_id={0}", Uri.EscapeDataString(userId));

            var strJson = Post(prefix, postfix, "/blocks/create.json", postbody);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<User>()
                {
                    IsSucceed = false,
                    Result = null,
                    Message = ""
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var user = User.Parse(jObj);

                response = new Response<User>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = user
                };
            }

            return response;
        }

        /// <summary>
        /// POST blocks/destroy
        /// https://api.twitter.com/1.1/blocks/create.json
        /// Blocks the specified user from following the authenticating user.
        /// </summary>
        /// <param name="screenName"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Response<User> PostBlocksDestroy(string screenName, string userId)
        {
            Response<User> response;

            var prefix = "";
            var postfix = !string.IsNullOrEmpty(screenName)
                ? string.Format("&screen_name={0}", Uri.EscapeDataString(screenName))
                : string.Format("&user_id={0}", Uri.EscapeDataString(userId));
            var postbody = !string.IsNullOrEmpty(screenName)
                ? string.Format("screen_name={0}", Uri.EscapeDataString(screenName))
                : string.Format("user_id={0}", Uri.EscapeDataString(userId));

            var strJson = Post(prefix, postfix, "/blocks/destroy.json", postbody);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<User>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var user = User.Parse(jObj);

                response = new Response<User>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = user
                };
            }

            return response;
        }

        /// <summary>
        /// GET users/show
        /// https://api.twitter.com/1.1/users/show.json
        /// Returns a variety of information about the user specified by the required user_id or screen_name parameter. 
        /// </summary>
        /// <param name="screenName"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserInfoResult GetUserInfo(string screenName, string userId)
        {
            UserInfoResult response;
            string prefix = "";
            string postfix = !string.IsNullOrEmpty(screenName)
                ? string.Format("&screen_name={0}", Uri.EscapeDataString(screenName))
                : string.Format("&user_id={0}", Uri.EscapeDataString(userId));
            string querystring = !string.IsNullOrEmpty(screenName)
                ? string.Format("?screen_name={0}", Uri.EscapeDataString(screenName))
                : string.Format("?user_id={0}", Uri.EscapeDataString(userId)); ;

            var strJson = Get(prefix, postfix, "/users/show.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new UserInfoResult()
                {
                    IsSucceed = false,
                    Message = "",
                    User = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var user = User.Parse(jObj);

                response = new UserInfoResult()
                {
                    IsSucceed = true,
                    Message = "",
                    User = user.ParseToDto()
                };
            }

            return response;
        }

        /// <summary>
        /// GET users/search
        /// https://api.twitter.com/1.1/users/search.json
        /// Provides a simple, relevance-based search interface to public user accounts on Twitter. 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="count"></param>
        /// <param name="page"></param>
        public UserSuggestResult GetUsersSearch(string query, int page = 1, int count = 20)
        {
            UserSuggestResult response;
            string prefix = string.Format("count={0}&", count, page);
            string postfix = string.Format("&q={0}", Uri.EscapeDataString(query));
            string querystring = string.Format("?q={0}&count={1}", Uri.EscapeDataString(query), count, page);

            var strJson = Get(prefix, postfix, "/users/search.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new UserSuggestResult()
                {
                    IsSucceed = false,
                    Message = "",
                    Users = new Dto.Base.User[0]
                };
            }
            else
            {
                var jArr = JArray.Parse(strJson);
                var users = User.ParseMany(jArr, count);

                response = new UserSuggestResult()
                {
                    IsSucceed = false,
                    Message = "",
                    Users = users.Select(x => x.ParseToDto()).ToArray()
                };
            }

            return response;
        }

        /// <summary>
        /// GET users/profile_banner
        /// https://api.twitter.com/1.1/users/profile_banner.json
        /// Returns a map of the available size variations of the specified user’s profile banner.
        /// </summary>
        /// <param name="screenName"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Response<string> GetUsersProfileBanner(string screenName, string userId)
        {
            Response<string> response;
            string prefix = "";
            string postfix = !string.IsNullOrEmpty(screenName)
                ? string.Format("&screen_name={0}", Uri.EscapeDataString(screenName))
                : string.Format("&user_id={0}", Uri.EscapeDataString(userId));

            string querystring = !string.IsNullOrEmpty(screenName)
                ? string.Format("?screen_name={0}", Uri.EscapeDataString(screenName))
                : string.Format("?user_id={0}", Uri.EscapeDataString(userId));

            var strJson = "";

            //Eger banner yoksa 404 donuyor
            try
            {
                strJson = Get(prefix, postfix, "/users/profile_banner.json", querystring);
                if (string.IsNullOrEmpty(strJson))
                {
                    response = new Response<string>()
                    {
                        IsSucceed = false,
                        Message = "",
                        Result = ""
                    };
                }
                else
                {
                    response = new Response<string>()
                    {
                        IsSucceed = true,
                        Message = "",
                        Result = strJson
                    };
                }
            }
            catch {
                response = new Response<string>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = ""
                };
            }

            return response;
        }

        /// <summary>
        /// GET favorites/list
        /// https://api.twitter.com/1.1/favorites/list.json
        /// Returns the 20 most recent Tweets liked by the authenticating or specified user.
        /// </summary>
        /// <param name="screenName"></param>
        /// <param name="userId"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public Response<List<Status>> GetFavoritesList(string screenName, string userId, int count = 20)
        {
            Response<List<Status>> response;

            var prefix = string.Format("count={0}&", count);
            var strJson = "";

            if (string.IsNullOrEmpty(screenName) && string.IsNullOrEmpty(userId))
                strJson = Get(prefix, "", "/favorites/list.json", "?count=" + count);
            else
            {
                strJson = (!string.IsNullOrEmpty(screenName))
                 ? Get(prefix, "&screen_name=" + Uri.EscapeDataString(screenName), "/favorites/list.json", "?screen_name=" + Uri.EscapeDataString(screenName) + "&count=" + count)
                 : Get(prefix, "&user_id=" + Uri.EscapeDataString(userId), "/favorites/list.json", "?user_id=" + Uri.EscapeDataString(userId) + "&count=" + count);
            }

            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<Status>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<Status>()
                };
            }
            else
            {
                var jArr = JArray.Parse(strJson);
                var list = Status.ParseMany(jArr);

                response = new Response<List<Status>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }

            return response;
        }

        /// <summary>
        /// POST favorites/destroy
        /// https://api.twitter.com/1.1/favorites/destroy.json
        /// Un-likes the status specified in the ID parameter as the authenticating user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response<Status> PostFavoritesDestroy(string id)
        {
            Response<Status> response;
            var prefix = string.Format("id={0}&", id);
            var postfix = "";
            var postbody = string.Format("id={0}", id);

            var strJson = Post(prefix, postfix, "/favorites/destroy.json", postbody);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<Status>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var status = Status.Parse(jObj);

                response = new Response<Status>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = status
                };
            }

            return response;
        }

        /// <summary>
        /// POST favorites/create
        /// https://api.twitter.com/1.1/favorites/create.json
        /// Likes the status specified in the ID parameter as the authenticating user. 
        /// <param name="id"></param>
        /// </summary>
        public Response<Status> PostFavoritesCreate(string id)
        {
            Response<Status> response;
            var prefix = string.Format("id={0}&", id);
            var postfix = "";
            var postbody = string.Format("id={0}", id);

            var strJson = Post(prefix, postfix, "/favorites/create.json", postbody);

            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<Status>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var status = Status.Parse(jObj);

                response = new Response<Status>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = status
                };
            }

            return response;
        }

        /// <summary>
        /// GET geo/search
        /// https://api.twitter.com/1.1/geo/search.json
        /// Search for places that can be attached to a statuses/update.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public Response<List<Place>> GetGeoSearch(string query)
        {
            Response<List<Place>> response;
            string prefix = "";
            string postfix = string.Format("&query={0}", Uri.EscapeDataString(query));
            string querystring = string.Format("?query={0}", Uri.EscapeDataString(query));

            var strJson = Get(prefix, postfix, "/geo/search.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<Place>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<Place>()
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var jArr = JArray.Parse(jObj["result"]["places"].ToString());
                var places = Place.ParseMany(jArr);

                response = new Response<List<Place>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = places
                };
            }

            return response;
        }

        /// <summary>
        /// GET geo/id/:place_id
        /// https://api.twitter.com/1.1/geo/id/:place_id.json
        /// Returns all the information about a known place. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response<Place> GetGeoShow(string id)
        {
            Response<Place> response;
            string prefix = "";
            string postfix = "";
            string querystring = "" ;

            var strJson = Get(prefix, postfix, "/geo/id/"+ id +".json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<Place>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = null
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var place = Place.Parse(jObj);

                response = new Response<Place>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = place
                };
            }

            return response;
        }

        /// <summary>
        /// GET geo/reverse_geocode
        /// https://api.twitter.com/1.1/geo/reverse_geocode.json
        /// Given a latitude and a longitude, searches for up to 20 places that can be used as a place_id when updating a status.
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lon"></param>
        /// <returns></returns>
        public Response<List<Place>> GetGeoReverseGeocode(string lat, string lon)
        {
            Response<List<Place>> response;
            string prefix = string.Format("lat={0}&long={1}&", lat, lon);
            string postfix = "";
            string querystring = string.Format("?lat={0}&long={1}", lat, lon);

            var strJson = Get(prefix, postfix, "/geo/reverse_geocode.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<Place>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<Place>()
                };
            }
            else
            {
                var jObj = JObject.Parse(strJson);
                var jArr = JArray.Parse(jObj["result"]["places"].ToString());
                var places = Place.ParseMany(jArr);

                response = new Response<List<Place>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = places
                };
            }

            return response;
        }

        /// <summary>
        /// GET trends/place
        /// Returns the top 50 trending topics for a specific WOEID, if trending information is available for it.
        /// WOEID 1 => World
        /// WOEID 23424969 => Turkey
        /// WOEID => https://developer.yahoo.com/geo/geoplanet/
        /// https://api.twitter.com/1.1/trends/place.json
        /// </summary>
        /// <returns></returns>
        public TrendResult GetTrendsPlace(string Woeid = "1")
        {
            TrendResult response;
            string prefix = string.Format("id={0}&", Woeid);
            string postfix = "";
            string querystring = string.Format("?id={0}", Woeid);

            var strJson = Get(prefix, postfix, "/trends/place.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new TrendResult()
                {
                    IsSucceed = false,
                    Message = "",
                    Trends = new List<Dto.Base.Trend>()
                };
            }
            else
            {
                JArray jArr = JArray.Parse(strJson);

                var list = new List<Trend>();
                if (jArr.Count > 0)
                {
                    JArray jArrTrends = JArray.Parse(jArr[0]["trends"].ToString());
                    list = Trend.ParseMany(jArrTrends);
                }

                response = new TrendResult() {
                    Trends = list.Select(i => i.ParseToDto()).ToList(), IsSucceed = true, Message = ""
                };
            }

            return response;
        }

        /// <summary>
        /// GET trends/available
        /// Returns the locations that Twitter has trending topic information for.
        /// WOEID => https://developer.yahoo.com/geo/geoplanet/
        /// https://api.twitter.com/1.1/trends/available.json
        /// </summary>
        /// <returns></returns>
        public Response<List<TrendLocation>> GetTrendsAvailable()
        {
            Response<List<TrendLocation>> response;

            string prefix = "";
            string postfix = "";
            string querystring = "";

            var strJson = Get(prefix, postfix, "/trends/available.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<TrendLocation>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<TrendLocation>()
                };
            }
            else
            {
                var jArr = JArray.Parse(strJson);
                var list = TrendLocation.ParseMany(jArr);

                response = new Response<List<TrendLocation>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }

            return response;
        }

        /// <summary>
        /// GET trends/closest
        /// https://api.twitter.com/1.1/trends/closest.json
        /// Returns the locations that Twitter has trending topic information for, closest to a specified location.
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lon"></param>
        /// <returns></returns>
        public Response<List<TrendLocation>> GetTrendsClosest(string lat, string lon)
        {
            Response<List<TrendLocation>> response;
            string prefix = string.Format("lat={0}&long={1}&", lat, lon);
            string postfix = "";
            string querystring = string.Format("?lat={0}&long={1}", lat, lon);

            var strJson = Get(prefix, postfix, "/trends/closest.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<List<TrendLocation>>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = new List<TrendLocation>()
                };
            }
            else
            {
                var jArr = JArray.Parse(strJson);
                var list = TrendLocation.ParseMany(jArr);

                response = new Response<List<TrendLocation>>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = list
                };
            }
           
            return response;
        }

        /// <summary>
        /// GET application/rate_limit_status
        /// https://api.twitter.com/1.1/application/rate_limit_status.json
        /// Returns the current rate limits for methods belonging to the specified resource families.
        /// </summary>
        /// <returns></returns>
        public Response<string> GetApplicationRateLimitStatus()
        {
            Response<string> response;

            string prefix = "";
            string postfix = "";
            string querystring = "";

            var strJson = Get(prefix, postfix, "/application/rate_limit_status.json", querystring);
            if (string.IsNullOrEmpty(strJson))
            {
                response = new Response<string>()
                {
                    IsSucceed = false,
                    Message = "",
                    Result = ""
                };
            }
            else
            {
                response = new Response<string>()
                {
                    IsSucceed = true,
                    Message = "",
                    Result = strJson
                };
            }

            return response;
        }

        #region UtilMethods

        public string Post(string baseStringPrefixParameters, string baseStringPostfixParameters, string method, string postBody)
        {
            var oauth_token = AccessToken;
            var oauth_token_secret = AccessTokenSecret;
            var oauth_consumer_key = ConsumerKey;
            var oauth_consumer_secret = ConsumerSecret;

            // oauth implementation details
            var oauth_version = "1.0";
            var oauth_signature_method = "HMAC-SHA1";

            // unique request details
            var oauth_nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            var timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();

            var resource_url = _baseApiUrl + method;

            // create oauth signature
            var baseFormat = "{6}oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
                            "&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}{7}";

            var baseString = string.Format(baseFormat,
                                        oauth_consumer_key,
                                        oauth_nonce,
                                        oauth_signature_method,
                                        oauth_timestamp,
                                        oauth_token,
                                        oauth_version,
                                        baseStringPrefixParameters,
                                        baseStringPostfixParameters);

            baseString = string.Concat("POST&", Uri.EscapeDataString(resource_url), "&", Uri.EscapeDataString(baseString));

            var compositeKey = string.Concat(Uri.EscapeDataString(oauth_consumer_secret),
                                    "&", Uri.EscapeDataString(oauth_token_secret));
            string oauth_signature;
            using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
            {
                oauth_signature = Convert.ToBase64String(
                    hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
            }

            // create the request header
            var headerFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
                               "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
                               "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
                               "oauth_version=\"{6}\"";

            var authHeader = string.Format(headerFormat,
                                    Uri.EscapeDataString(oauth_nonce),
                                    Uri.EscapeDataString(oauth_signature_method),
                                    Uri.EscapeDataString(oauth_timestamp),
                                    Uri.EscapeDataString(oauth_consumer_key),
                                    Uri.EscapeDataString(oauth_token),
                                    Uri.EscapeDataString(oauth_signature),
                                    Uri.EscapeDataString(oauth_version)
                            );
            // make the request
            ServicePointManager.Expect100Continue = false;
            string objText = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource_url);
                request.Headers.Add("Authorization", authHeader);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                using (var stream = request.GetRequestStream())
                {
                    var bytes = ASCIIEncoding.ASCII.GetBytes(postBody);
                    stream.Write(bytes, 0, bytes.Length);
                }

                WebResponse response = request.GetResponse();
                var reader = new StreamReader(response.GetResponseStream());
               objText = reader.ReadToEnd();
            }
            catch {
                objText = "";
            }

            return objText;
        }

        public string Get(string baseStringPrefixParameters, string baseStringPostfixParameters,string method,string querystring)
        {
            ServicePointManager.Expect100Continue = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;

            // oauth application keys
            var oauth_token = AccessToken;
            var oauth_token_secret = AccessTokenSecret;
            var oauth_consumer_key = ConsumerKey;
            var oauth_consumer_secret = ConsumerSecret; 

            // oauth implementation details
            var oauth_version = "1.0";
            var oauth_signature_method = "HMAC-SHA1";

            // unique request details
            var oauth_nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            var timeSpan = DateTime.UtcNow
                - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds + 305).ToString();

            // create oauth signature
            var baseFormat = "{6}oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
                            "&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}{7}";

            var baseString = string.Format(baseFormat,
                                        oauth_consumer_key,
                                        oauth_nonce,
                                        oauth_signature_method,
                                        oauth_timestamp,
                                        oauth_token,
                                        oauth_version,
                                        baseStringPrefixParameters,
                                        baseStringPostfixParameters);

            baseString = "GET&" + Uri.EscapeDataString(_baseApiUrl + method) + "&" + Uri.EscapeDataString(baseString);

            var compositeKey = string.Concat(Uri.EscapeDataString(oauth_consumer_secret),
                                    "&", Uri.EscapeDataString(oauth_token_secret));

            string oauth_signature;
            using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
            {
                oauth_signature = Convert.ToBase64String(
                    hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
            }

            // create the request header
            var headerFormat = "OAuth oauth_consumer_key=\"{3}\", oauth_nonce=\"{0}\", " +
                               "oauth_signature=\"{5}\", oauth_signature_method=\"{1}\", " +
                               "oauth_timestamp=\"{2}\", oauth_token=\"{4}\", " +
                               "oauth_version=\"{6}\"";

            var authHeader = string.Format(headerFormat,
                                    Uri.EscapeDataString(oauth_nonce),//0
                                    Uri.EscapeDataString(oauth_signature_method),//1
                                    Uri.EscapeDataString(oauth_timestamp),//2
                                    Uri.EscapeDataString(oauth_consumer_key),//3
                                    Uri.EscapeDataString(oauth_token),//4
                                    Uri.EscapeDataString(oauth_signature),//5
                                    Uri.EscapeDataString(oauth_version)//6
                            );



            ServicePointManager.Expect100Continue = false;
            string objText = "";
            try
            {
                // make the request
                var resource_url = _baseApiUrl + method + querystring;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource_url);
                request.Headers.Add("Authorization", authHeader);
                request.Method = "GET";
                request.ContentType = "application/x-www-form-urlencoded";
                var response = (HttpWebResponse)request.GetResponse();
                var reader = new StreamReader(response.GetResponseStream());
                objText = reader.ReadToEnd();
            }
            catch(Exception exc) {
                objText = "";
            }

            return objText;
        }

        #endregion
    }
}
