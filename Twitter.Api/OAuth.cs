﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Twitter.Api.Entities;

namespace Twitter.Api
{
    public class Oauth
    {
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }

        public Oauth(string consumerKey, string consumerSecret)
        {
            ConsumerKey = consumerKey;
            ConsumerSecret = consumerSecret;
        }

        public XAuthResponse Login(string userName, string password)
        {
            string prefix = "";
            string postfix = string.Format("&x_auth_mode=client_auth&x_auth_password={0}&x_auth_username={1}", Uri.EscapeDataString(password), Uri.EscapeDataString(userName));
            string postbody = string.Format("x_auth_mode=client_auth&x_auth_password={0}&x_auth_username={1}", Uri.EscapeDataString(password), Uri.EscapeDataString(userName));

            var strResponse = Post(prefix, postfix, postbody);

            if (string.IsNullOrEmpty(strResponse))
                return null;

            var arr = strResponse.Split('&');
            var xAuthResponse = new XAuthResponse();

            foreach (var str in arr)
            {
                var tempArr = str.Split('=');
                switch (tempArr[0])
                {
                    case "oauth_token": xAuthResponse.OAuthToken = tempArr[1]; break;
                    case "oauth_token_secret": xAuthResponse.OAuthTokenSecret = tempArr[1]; break;
                    case "user_id": xAuthResponse.UserId = tempArr[1]; break;
                    case "screen_name": xAuthResponse.ScreenName = tempArr[1]; break;
                }
            }

            return xAuthResponse;
        }

        public string Post(string baseStringPrefixParameters, string baseStringPostfixParameters, string postBody)
        {
            ServicePointManager.Expect100Continue = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;

            var oauth_consumer_key = ConsumerKey;
            var oauth_consumer_secret = ConsumerSecret;

            // oauth implementation details
            var oauth_version = "1.0";
            var oauth_signature_method = "HMAC-SHA1";

            // unique request details
            var oauth_nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            var timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();

            var resource_url = "https://api.twitter.com/oauth/access_token";

            // create oauth signature
            var baseFormat = "{5}oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
                            "&oauth_timestamp={3}&oauth_version={4}{6}";

            var baseString = string.Format(baseFormat,
                                        oauth_consumer_key,
                                        oauth_nonce,
                                        oauth_signature_method,
                                        oauth_timestamp,
                                        oauth_version,
                                        baseStringPrefixParameters,
                                        baseStringPostfixParameters);

            baseString = string.Concat("POST&", Uri.EscapeDataString(resource_url), "&", Uri.EscapeDataString(baseString));

            var compositeKey = string.Concat(Uri.EscapeDataString(oauth_consumer_secret), "&");
            string oauth_signature;
            using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
            {
                oauth_signature = Convert.ToBase64String(
                    hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
            }

            // create the request header
            var headerFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
                               "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
                               "oauth_signature=\"{4}\", " +
                               "oauth_version=\"{5}\"";

            var authHeader = string.Format(headerFormat,
                                    Uri.EscapeDataString(oauth_nonce),
                                    Uri.EscapeDataString(oauth_signature_method),
                                    Uri.EscapeDataString(oauth_timestamp),
                                    Uri.EscapeDataString(oauth_consumer_key),
                                    Uri.EscapeDataString(oauth_signature),
                                    Uri.EscapeDataString(oauth_version));

            // make the request
            ServicePointManager.Expect100Continue = false;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource_url);
            request.Headers.Add("Authorization", authHeader);
            request.UserAgent = "MyAppId/1.0.3 +http://example.pt";
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            using (var stream = request.GetRequestStream())
            {
                var bytes = ASCIIEncoding.ASCII.GetBytes(postBody);
                stream.Write(bytes, 0, bytes.Length);
            }

            string objText = "";

            try
            {
                WebResponse response = request.GetResponse();
                var reader = new StreamReader(response.GetResponseStream());
                objText = reader.ReadToEnd();
            }
            catch(Exception exc)
            {
            }

            return objText;
        }
    }
}
