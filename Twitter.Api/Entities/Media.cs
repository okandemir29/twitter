﻿namespace Twitter.Api.Entities
{
    using Extensions;
    using Newtonsoft.Json.Linq;

    public class Media
    {
        public string Id { get; set; }
        public string MediaUrl { get; set; }
        public string MediaUrlHttps { get; set; }
        public string VideoUrl { get; set; }
        public string Url { get; set; }
        public string ExpandedUrl { get; set; }
        public string Type { get; set; }
        public string SourceStatusId { get; set; }
        public string SourceUserId { get; set; }

        public static Media Parse(JObject jObj)
        {
            var media = new Media();

            media.Id = jObj.MapToString("id");
            media.MediaUrl = jObj.MapToString("media_url");
            media.MediaUrlHttps = jObj.MapToString("media_url_https");
            media.Url = jObj.MapToString("url");
            media.ExpandedUrl = jObj.MapToString("expanded_url");
            media.Type = jObj.MapToString("type");
            media.SourceStatusId = jObj.MapToString("source_status_id");
            media.SourceUserId = jObj.MapToString("source_user_id");
            media.VideoUrl = "";

            if (jObj["video_info"] != null && jObj["video_info"]["variants"] != null)
            {
                var jArrVariants = JArray.Parse(jObj["video_info"]["variants"].ToString());
                if (jArrVariants.Count > 0)
                {
                    foreach (JObject jVariant in jArrVariants)
                    {
                        var extension = jVariant["content_type"].ToString();
                        if (extension == "video/mp4")
                        {
                            media.VideoUrl = jVariant["url"].ToString();
                            media.Type = "video";

                            break;
                        }
                    }
                }
            }

            return media;
        }

        public Dto.Base.Media ParseToDto()
        {
            var media = new Dto.Base.Media();

            media.Id = Id;
            media.ExpandedUrl = ExpandedUrl;
            media.MediaUrl = MediaUrl;
            media.MediaUrlHttps = MediaUrlHttps;
            media.SourceStatusId = SourceStatusId;
            media.SourceUserId = SourceUserId;
            media.Type = Type;
            media.Url = Url;
            media.VideoUrl = VideoUrl;

            return media;
        }
    }
}
