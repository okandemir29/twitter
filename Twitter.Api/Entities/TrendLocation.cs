﻿namespace Twitter.Api.Entities
{
    using Extensions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;

    public class TrendLocation
    {
        public string Name { get; set; }
        public string CountryCode { get; set; }
        public string Url { get; set; }
        public string Woeid { get; set; }
        public string Country { get; set; }
        public string PlaceType { get; set; }

        public static TrendLocation Parse(JObject jObj)
        {
            var loc = new TrendLocation();

            loc.Name = jObj.MapToString("name");
            loc.CountryCode = jObj.MapToString("countryCode");
            loc.Url = jObj.MapToString("url");
            loc.Woeid = jObj.MapToString("woeid");
            loc.Country = jObj.MapToString("country");

            if (jObj["placeType"] != null)
                loc.PlaceType = jObj["placeType"]["name"].ToString();

            return loc;
        }

        public static List<TrendLocation> ParseMany(JArray jArr)
        {
            var list = new List<TrendLocation>();

            foreach (JObject jObj in jArr)
            {
                list.Add(Parse(jObj));
            }

            return list;
        }
    }
}
