﻿namespace Twitter.Api.Entities
{
    using Extensions;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;

    public class UserSimple
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ScreenName { get; set; }
        public string Description { get; set; }
        public bool Protected { get; set; }
        public int FollowersCount { get; set; }
        public int FriendsCount { get; set; }
        public string ImageUrl { get; set; }

        public UserSimple()
        {
            Protected = true;
            FollowersCount = 0;
            FriendsCount = 0;
        }

        public static UserSimple Parse(JObject jObj)
        {
            var user = new UserSimple();

            user.Id = jObj.MapToString("id");
            user.Name = jObj.MapToString("name");
            user.ScreenName = jObj.MapToString("screen_name").ToLowerCulture();
            user.Description = jObj.MapToString("description");
            user.Protected = jObj.MapToBool("protected");
            user.FollowersCount = jObj.MapToInt("followers_count");
            user.FriendsCount = jObj.MapToInt("friends_count");
            user.ImageUrl = jObj.MapToString("profile_image_url");

            return user;
        }

        public static List<UserSimple> ParseMany(JArray jArr, int limit = 100)
        {
            var list = new List<UserSimple>();
            int counter = 0;

            foreach (JObject jObj in jArr)
            {
                list.Add(Parse(jObj));
                counter += 1;

                if (counter >= limit)
                    break;
            }

            return list;
        }

        public Dto.Base.UserSimple ParseToDto()
        {
            var user = new Dto.Base.UserSimple();

            user.Description = Description;
            user.FollowersCount = FollowersCount;
            user.FriendsCount = FriendsCount;
            user.Id = Id;
            user.Name = Name;
            user.Protected = Protected;
            user.ScreenName = ScreenName;
            user.ImageUrl = ImageUrl;

            return user;
        }
    }
}
