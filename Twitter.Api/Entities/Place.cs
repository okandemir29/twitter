﻿namespace Twitter.Api.Entities
{
    using Extensions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;

    public class Place
    {
        public string Id { get; set; }
        public string PlaceType { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string CountryCode { get; set; }
        public string Country { get; set; }

        public static Place Parse(JObject jObj)
        {
            if (jObj == null)
                return null;

            var place = new Place();

            place.Id = jObj.MapToString("id");
            place.PlaceType = jObj.MapToString("place_type");
            place.Name = jObj.MapToString("name");
            place.FullName = jObj.MapToString("full_name");
            place.CountryCode = jObj.MapToString("country_code");
            place.Country = jObj.MapToString("country");

            return place;
        }

        public static List<Place> ParseMany(JArray jArr)
        {
            var list = new List<Place>();

            foreach (JObject jObj in jArr)
            {
                list.Add(Parse(jObj));
            }

            return list;
        }

        public Dto.Base.Place ParseToDto()
        {
            var place = new Dto.Base.Place();

            place.Country = Country;
            place.CountryCode = CountryCode;
            place.FullName = FullName;
            place.Id = Id;
            place.Name = Name;
            place.PlaceType = PlaceType;

            return place;
        }
    }
}
