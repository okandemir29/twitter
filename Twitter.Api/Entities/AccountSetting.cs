﻿namespace Twitter.Api.Entities
{
    using Extensions;
    using Newtonsoft.Json.Linq;

    public class AccountSetting
    {
        public bool Protected { get; set; }
        public string ScreenName { get; set; }
        public bool AlwaysUseHttps { get; set; }
        public string Language { get; set; }
        public bool DiscoverableByEmail { get; set; }
        public bool DiscoverableByMobilePhone { get; set; }
        public bool DisplaySensitiveMedia { get; set; }
        public TrendLocation TrendLocation { get; set; }


        public static AccountSetting Parse(JObject jObj)
        {
            var sett = new AccountSetting();

            sett.Protected = jObj.MapToBool("protected");
            sett.ScreenName = jObj.MapToString("screen_name");
            sett.AlwaysUseHttps = jObj.MapToBool("always_use_https");
            sett.Language = jObj.MapToString("language");
            sett.DiscoverableByEmail = jObj.MapToBool("discoverable_by_email");
            sett.DiscoverableByMobilePhone = jObj.MapToBool("discoverable_by_mobile_phone");
            sett.DisplaySensitiveMedia = jObj.MapToBool("display_sensitive_media");

            if (jObj["trend_location"] != null)
            {
                var jArr = JArray.Parse(jObj["trend_location"].ToString());
                if (jArr.Count > 0)
                    sett.TrendLocation = TrendLocation.Parse(jArr[0] as JObject);
            }

            return sett;
        }
    }
}
