﻿namespace Twitter.Api.Entities
{
    using Extensions;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;

    public class User
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ScreenName { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public string ProfileUrl { get; set; }
        public string Url { get; set; }
        public bool Protected { get; set; }
        public int FollowersCount { get; set; }
        public int FriendsCount { get; set; }
        public DateTime CreatedAt { get; set; }
        public int FavouritesCount { get; set; }
        public bool Verified { get; set; }
        public int StatusesCount { get; set; }
        public string Lang { get; set; }
        public bool IsTranslator { get; set; }
        public string BackgroundColor { get; set; }
        public string BackgroundImageUrl { get; set; }
        public string BackgroundImageUrlHttps { get; set; }
        public string BackgroundTile { get; set; }
        public string ImageUrl { get; set; }
        public string ImageUrlHttps { get; set; }
        public string BannerUrl { get; set; }
        public string LinkColor { get; set; }
        public string SidebarBorderColor { get; set; }
        public string SidebarFillColor { get; set; }
        public string TextColor { get; set; }
        public bool DefaultProfile { get; set; }
        public bool DefaultProfileImage { get; set; }

        public User()
        {
            Url = Url;
            Protected = true;
            FollowersCount = 0;
            FriendsCount = 0;
            FavouritesCount = 0;
            Verified = false;
            StatusesCount = 0;
            IsTranslator = false;
            DefaultProfile = true;
            DefaultProfileImage = true;
        }

        public static User Parse(JObject jObj)
        {
            var user = new User();

            user.Id = jObj.MapToString("id");
            user.Name = jObj.MapToString("name");
            user.ScreenName = jObj.MapToString("screen_name").ToLowerCulture();
            user.Location = jObj.MapToString("location");
            user.Description = jObj.MapToString("description");
            user.ProfileUrl = jObj.MapToString("url");

            if (jObj["entities"] != null)
            {
                if (jObj["entities"]["url"] != null && jObj["entities"]["url"]["urls"] != null)
                {
                    JArray jArr = JArray.Parse(jObj["entities"]["url"]["urls"].ToString());

                    user.Url = jArr[0]["expanded_url"].ToString();
                }
            }

            user.Protected = jObj.MapToBool("protected");
            user.FollowersCount = jObj.MapToInt("followers_count");
            user.FriendsCount = jObj.MapToInt("friends_count");
            user.CreatedAt = DateTime.ParseExact(jObj["created_at"].ToString(), "ddd MMM dd HH:mm:ss +ffff yyyy", new System.Globalization.CultureInfo("en-US"));
            user.FavouritesCount = jObj.MapToInt("favourites_count");
            user.Verified = jObj.MapToBool("verified");
            user.StatusesCount = jObj.MapToInt("statuses_count");
            user.Lang = jObj.MapToString("lang");
            user.IsTranslator = jObj.MapToBool("is_translator");
            user.BackgroundColor = jObj.MapToString("profile_background_color");
            user.BackgroundImageUrl = jObj.MapToString("profile_background_image_url");
            user.BackgroundImageUrlHttps = jObj.MapToString("profile_background_image_url_https");
            user.BackgroundTile = jObj.MapToString("profile_background_tile");
            user.ImageUrl = jObj.MapToString("profile_image_url");
            user.ImageUrlHttps = jObj.MapToString("profile_image_url_https");
            user.LinkColor = jObj.MapToString("profile_link_color");
            user.SidebarBorderColor = jObj.MapToString("profile_sidebar_border_color");
            user.SidebarFillColor = jObj.MapToString("profile_sidebar_fill_color");
            user.TextColor = jObj.MapToString("profile_text_color");
            user.DefaultProfile = jObj.MapToBool("default_profile");
            user.DefaultProfileImage = jObj.MapToBool("default_profile_image");
            user.BannerUrl = jObj.MapToString("profile_banner_url");

            return user;
        }

        public static List<User> ParseMany(JArray jArr, int limit = 100)
        {
            var list = new List<User>();
            int counter = 0;

            foreach (JObject jObj in jArr)
            {
                list.Add(Parse(jObj));
                counter += 1;

                if (counter >= limit)
                    break;
            }

            return list;
        }

        public Dto.Base.User ParseToDto()
        {
            var user = new Dto.Base.User();

            user.BackgroundColor = BackgroundColor;
            user.BackgroundImageUrl = BackgroundImageUrl;
            user.BackgroundImageUrlHttps = BackgroundImageUrlHttps;
            user.BackgroundTile = BackgroundTile;
            user.BannerUrl = BannerUrl;
            user.CreatedAt = CreatedAt;
            user.DefaultProfile = DefaultProfile;
            user.DefaultProfileImage = DefaultProfileImage;
            user.Description = Description;
            user.FavouritesCount = FavouritesCount;
            user.FollowersCount = FollowersCount;
            user.FriendsCount = FriendsCount;
            user.Id = Id;
            user.ImageUrl = ImageUrl;
            user.ImageUrlHttps = ImageUrlHttps;
            user.IsTranslator = IsTranslator;
            user.Lang = Lang;
            user.LinkColor = LinkColor;
            user.Location = Location;
            user.Name = Name;
            user.ProfileUrl = ProfileUrl;
            user.Protected = Protected;
            user.ScreenName = ScreenName;
            user.SidebarBorderColor = SidebarBorderColor;
            user.SidebarFillColor = SidebarFillColor;
            user.StatusesCount = StatusesCount;
            user.TextColor = TextColor;
            user.Url = Url;
            user.Verified = Verified;

            return user;
        }
    }
}
