﻿namespace Twitter.Api.Entities
{
    using Extensions;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Status
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public User User { get; set; }
        public int RetweetCount { get; set; }
        public int FavoriteCount { get; set; }
        public string Lang { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<string> Hashtags { get; set; }
        public List<string> Urls { get; set; }
        public List<UserMention> UserMentions { get; set; }
        public List<Media> Medias { get; set; }
        public Place Place { get; set; }
        public Retweeted Retweeted { get; set; }


        public Status()
        {
            Hashtags = new List<string>();
            Urls = new List<string>();
            UserMentions = new List<UserMention>();
            Medias = new List<Media>();
        }

        public static Status Parse(JObject jObj)
        {
            var status = new Status();

            if (jObj["created_at"] != null)
                status.CreatedAt = DateTime.ParseExact(jObj["created_at"].ToString(), "ddd MMM dd HH:mm:ss +ffff yyyy", new System.Globalization.CultureInfo("en-US"));

            status.Id = jObj.MapToString("id");
            status.Text = jObj.MapToString("text");
            if (string.IsNullOrEmpty(status.Text)) {
                status.Text = jObj.MapToString("full_text");
            }
            status.Lang = jObj.MapToString("lang");

            if (jObj["user"] != null)
                status.User = User.Parse(jObj["user"] as JObject);

            if (jObj["extended_entities"] != null)
            {
                if (jObj["extended_entities"]["media"] != null)
                {
                    JArray jArr = JArray.Parse(jObj["extended_entities"]["media"].ToString());
                    foreach (JObject x in jArr)
                        status.Medias.Add(Media.Parse(x));
                }
            }

            if (jObj["entities"] != null)
            {
                if (jObj["entities"]["hashtags"] != null)
                {
                    JArray jArr = JArray.Parse(jObj["entities"]["hashtags"].ToString());

                    foreach (var x in jArr)
                    {
                        var tag = x["text"].ToString();

                        if (!status.Hashtags.Contains(tag))
                             status.Hashtags.Add(tag.ToLowerCulture());
                    }
                }

                if (jObj["entities"]["user_mentions"] != null)
                {
                    JArray jArr = JArray.Parse(jObj["entities"]["user_mentions"].ToString());

                    foreach (JObject x in jArr)
                    {
                        var mention = UserMention.Parse(x);

                        if(!status.UserMentions.Any(y => y.ScreenName.Equals(mention.ScreenName)))
                             status.UserMentions.Add(mention);
                    }
                }

                if (jObj["entities"]["urls"] != null)
                {
                    JArray jArr = JArray.Parse(jObj["entities"]["urls"].ToString());

                    foreach (var x in jArr)
                        status.Urls.Add(x["url"].ToString());
                }
            }

            if (jObj["place"] != null)
                status.Place = Place.Parse(jObj["place"] as JObject);

            if (jObj["retweet_count"] != null)
            {
                int retweetCount = 0;
                if (!int.TryParse(jObj["retweet_count"].ToString(), out retweetCount))
                    retweetCount = 0;
                 
                status.RetweetCount = retweetCount;
            }

            if (jObj["favorite_count"] != null)
            {
                int favoriteCount = 0;
                if (!int.TryParse(jObj["favorite_count"].ToString(), out favoriteCount))
                    favoriteCount = 0;

                status.FavoriteCount = favoriteCount;
            }

            if (jObj["retweeted_status"] != null)
                status.Retweeted = Retweeted.Parse(jObj["retweeted_status"] as JObject);


            return status;
        }

        public static List<Status> ParseMany(JArray jArr)
        {
            var list = new List<Status>();

            foreach (JObject jObj in jArr)
            {
                list.Add(Parse(jObj));
            }

            return list;
        }

        public Dto.Base.Status ParseToDto()
        {
            var status = new Dto.Base.Status();

            status.CreatedAt = CreatedAt;
            status.FavoriteCount = FavoriteCount;
            status.Hashtags = Hashtags;
            status.Id = Id;
            status.Lang = Lang;
            status.Medias = Medias.Select(x => x.ParseToDto()).ToList();
            status.Place = Place == null ? null : Place.ParseToDto();
            status.RetweetCount = RetweetCount;
            status.Text = Text;
            status.Urls = Urls;
            status.User = User.ParseToDto();
            status.UserMentions = UserMentions.Select(x => x.ParseToDto()).ToList();
            status.TimeAgo = CreatedAt.ToTimeAgo();
            status.Retweeted = Retweeted == null ? null : Retweeted.ParseToDto();

            return status;
        }
    }
}
