﻿namespace Twitter.Api.Entities
{
    using Extensions;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;

    public class DirectMessage
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public User Sender { get; set; }
        public string SenderId { get; set; }
        public User Recipient { get; set; }
        public string RecipientId { get; set; }
        public DateTime CreatedAt { get; set; }

        public static DirectMessage Parse(JObject jObj)
        {
            var message = new DirectMessage();

            message.Id = jObj.MapToString("id");
            message.Text = jObj.MapToString("text");
            message.SenderId = jObj.MapToString("sender_id");
            message.RecipientId = jObj.MapToString("recipient_id");

            if (jObj["sender"] != null)
                message.Sender = User.Parse(jObj["sender"] as JObject);

            if (jObj["recipient"] != null)
                message.Recipient = User.Parse(jObj["recipient"] as JObject);

            if (jObj["created_at"] != null)
                message.CreatedAt = DateTime.ParseExact(jObj["created_at"].ToString(), "ddd MMM dd HH:mm:ss +ffff yyyy", new System.Globalization.CultureInfo("en-US"));

            return message;
        }

        public static List<DirectMessage> ParseMany(JArray jArr)
        {
            var list = new List<DirectMessage>();

            foreach (JObject jObj in jArr)
            {
                list.Add(Parse(jObj));
            }

            return list;
        }
    }
}
