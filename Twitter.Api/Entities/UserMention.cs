﻿namespace Twitter.Api.Entities
{
    using Extensions;
    using Newtonsoft.Json.Linq;

    public class UserMention
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ScreenName { get; set; }

        public static UserMention Parse(JObject jObj)
        {
            var userMention = new UserMention();

            userMention.Id = jObj.MapToString("id");
            userMention.Name = jObj.MapToString("name");
            userMention.ScreenName = jObj.MapToString("screen_name");

            return userMention;
        }

        public Dto.Base.UserMention ParseToDto()
        {
            var mention = new Dto.Base.UserMention();

            mention.Id = Id;
            mention.Name = Name;
            mention.ScreenName = ScreenName;

            return mention;
        }
    }
}
