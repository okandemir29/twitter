﻿namespace Twitter.Api.Entities
{
    using Extensions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;

    public class Trend
    {
        public string Name { get; set; }
        public string Query { get; set; }
        public int Volume { get; set; }

        public static Trend Parse(JObject jObj)
        {
            var loc = new Trend();

            loc.Name = jObj.MapToString("name");
            loc.Query = jObj.MapToString("query");

            if (jObj["tweet_volume"] != null)
            {
                int volume = 0;
                if (!int.TryParse(jObj["tweet_volume"].ToString(), out volume))
                    volume = 0;

                loc.Volume = volume;
            }

            return loc;
        }

        public static List<Trend> ParseMany(JArray jArr)
        {
            var list = new List<Trend>();

            foreach (JObject jObj in jArr)
            {
                list.Add(Parse(jObj));
            }

            return list;
        }

        public Dto.Base.Trend ParseToDto()
        {
            var trend = new Dto.Base.Trend();

            trend.Name = Name;
            trend.Query = Query;
            trend.Volume = Volume;

            return trend;
        }
    }
}
