﻿namespace Twitter.Welcome
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Twitter.Data;
    using Twitter.Model;
    using System.Web;
    using System.Web.Caching;

    public class ApiHelper
    {
        private static int _siteId;

        public ApiHelper()
        {

        }

        public ApiHelper(int siteId)
        {
            _siteId = siteId;
        }

        public Token GetRandomToken()
        {
            if (_siteId == 0)
            {
                return new Token()
                {
                    AccessSecret = "9WDJw3g0usHcVGqXISrADdcqe72vTTSYWOOAWK8i0AFfc",
                    AccessToken = "731578755454541829-K4cmKeaoGs8RBJwPCYA0BbnjAE6BYLV",
                    ConsumerKey = "yT577ApRtZw51q4NPMPPOQ",
                    ConsumerSecret = "3neq3XqN5fO3obqwZoajavGFCUrC42ZfbrLXy5sCv8",
                    IsClosedByService = false
                };
            }

            //cache'de var mi, yoksa cache'e ekle
            if (HttpContext.Current.Cache["Welcome_Tokens"] == null)
            {
                var tokens = new TokenData()
                    .GetBy(x => x.SiteId == _siteId && !x.IsClosedByService);

                HttpContext.Current.Cache.Add(
                      "Welcome_Tokens"
                    , tokens
                    , null
                    , DateTime.Now.AddDays(1)
                    , Cache.NoSlidingExpiration
                    , CacheItemPriority.AboveNormal
                    , null);
            }

            return (HttpContext.Current.Cache["Welcome_Tokens"] as List<Token>).OrderBy(x => Guid.NewGuid()).FirstOrDefault();
        }

        public static bool ClearTokensCache()
        {
            HttpContext.Current.Cache.Remove("Welcome_Tokens");

            return true;
        }

        public Dto.TagFeedResult GetStatusesByTag(string tag, int tryCount = 3, int count = 20, string maxId = "")
        {
            //tokenları alalım
            var token = GetRandomToken();
            var api = new Api.Gatherer(token.ConsumerKey, token.ConsumerSecret, token.AccessToken, token.AccessSecret);

            int counter = 0;
            Dto.TagFeedResult apiResult = new Dto.TagFeedResult()
            {
                IsSucceed = false
            };

            while (counter < tryCount && !apiResult.IsSucceed)
            {
                try
                {
                    apiResult = api.GetStatusesByTag(tag, count, maxId);
                }
                catch
                {
                }

                //token'ı yenile
                if (apiResult.Statuses == null || !apiResult.IsSucceed)
                {
                    token = GetRandomToken();
                    api = new Api.Gatherer(token.ConsumerKey, token.ConsumerSecret, token.AccessToken, token.AccessSecret);
                }

                counter += 1;
            }

            //donguden ciktiktan sonra hala isSucceed olmayabilir o zmn cevap vermesin
            return apiResult;
        }

        public Dto.StatusResult GetStatusById(string statusId, int tryCount = 3)
        {
            //tokenları alalım
            var token = GetRandomToken();
            var api = new Api.Gatherer(token.ConsumerKey, token.ConsumerSecret, token.AccessToken, token.AccessSecret);

            int counter = 0;
            var apiResult = new Dto.StatusResult()
            {
                IsSucceed = false
            };

            while (counter < tryCount && !apiResult.IsSucceed)
            {
                try
                {
                    apiResult = api.GetStatusById(statusId);
                }
                catch
                {
                }

                //token'ı yenile
                if (apiResult.Status == null || !apiResult.IsSucceed)
                {
                    token = GetRandomToken();
                    api = new Api.Gatherer(token.ConsumerKey, token.ConsumerSecret, token.AccessToken, token.AccessSecret);
                }

                counter += 1;
            }

            //donguden ciktiktan sonra hala isSucceed olmayabilir o zmn cevap vermesin
            return apiResult;
        }

        public Dto.UserFeedResult GetStatusesByUsername(string userName, int tryCount = 3, string maxId = "")
        {
            //tokenları alalım
            var token = GetRandomToken();
            var api = new Api.Gatherer(token.ConsumerKey, token.ConsumerSecret, token.AccessToken, token.AccessSecret);
            int counter = 0;
            var apiResult = new Dto.UserFeedResult()
            {
                IsSucceed = false
            };

            while (counter < tryCount && !apiResult.IsSucceed)
            {
                try
                {
                    apiResult = api.GetStatusesByUsername(userName, "", 20, maxId);
                }
                catch (Exception exc)
                {
                }

                //token'ı yenile
                if (apiResult == null || !apiResult.IsSucceed)
                {
                    token = GetRandomToken();
                    api = new Api.Gatherer(token.ConsumerKey, token.ConsumerSecret, token.AccessToken, token.AccessSecret);
                }

                counter += 1;
            }

            //donguden ciktiktan sonra hala isSucceed olmayabilir o zmn cevap vermesin
            return apiResult;
        }

        public Dto.UserSuggestResult GetUserSuggestions(string user, int tryCount = 3)
        {
            var token = GetRandomToken();
            var api = new Api.Gatherer(token.ConsumerKey, token.ConsumerSecret, token.AccessToken, token.AccessSecret);
            var counter = 0;
            var apiResult = new Dto.UserSuggestResult()
            {
                IsSucceed = false
            };

            while (counter < tryCount && !apiResult.IsSucceed)
            {
                apiResult = api.GetUsersSearch(user);

                //token'ı yenile
                if (!apiResult.IsSucceed)
                {
                    token = GetRandomToken();
                    api = new Api.Gatherer(token.ConsumerKey, token.ConsumerSecret, token.AccessToken, token.AccessSecret);
                    //muhtemelen log'da atmamiz gerekecek
                }

                counter += 1;
            }

            //donguden ciktiktan sonra hala isSucceed olmayabilir o zmn cevap vermesin
            return apiResult;
        }

        public Dto.TagSuggestResult GetTagSuggestions(string tag, int tryCount = 3)
        {
            return new Dto.TagSuggestResult()
            {
                IsSucceed = true,
                Message = "",
                Tags = new List<Dto.SearchTag>()
            };
        }

        public Dto.UserFriendshipResult GetUserFollowers(string userName, int tryCount = 1)
        {
            //tokenları alalım
            var token = GetRandomToken();
            var api = new Api.Gatherer(token.ConsumerKey, token.ConsumerSecret, token.AccessToken, token.AccessSecret);

            var counter = 0;
            var apiResult = new Dto.UserFriendshipResult()
            {
                IsSucceed = false
            };

            while (counter < tryCount && !apiResult.IsSucceed)
            {
                apiResult = api.GetFollowersList(userName, "");

                //token'ı yenile
                if (!apiResult.IsSucceed)
                {
                    token = GetRandomToken();
                    api = new Api.Gatherer(token.ConsumerKey, token.ConsumerSecret, token.AccessToken, token.AccessSecret);
                    //muhtemelen log'da atmamiz gerekecek
                }

                counter += 1;
            }

            //donguden ciktiktan sonra hala isSucceed olmayabilir o zmn cevap vermesin
            return apiResult;
        }

        public Dto.UserFriendshipResult GetUserFollowings(string userName, int tryCount = 1)
        {
            //tokenları alalım
            var token = GetRandomToken();
            var api = new Api.Gatherer(token.ConsumerKey, token.ConsumerSecret, token.AccessToken, token.AccessSecret);

            var counter = 0;
            var apiResult = new Dto.UserFriendshipResult()
            {
                IsSucceed = false
            };

            while (counter < tryCount && !apiResult.IsSucceed)
            {
                apiResult = api.GetFriendsList(userName, "");

                //token'ı yenile
                if (!apiResult.IsSucceed)
                {
                    token = GetRandomToken();
                    api = new Api.Gatherer(token.ConsumerKey, token.ConsumerSecret, token.AccessToken, token.AccessSecret);
                    //muhtemelen log'da atmamiz gerekecek
                }

                counter += 1;
            }

            //donguden ciktiktan sonra hala isSucceed olmayabilir o zmn cevap vermesin
            return apiResult;
        }

        public Dto.UserInfoResult GetUserInfo(string userName, int tryCount = 3)
        {
            //tokenları alalım
            var token = GetRandomToken();

            var api = new Api.Gatherer(token.ConsumerKey, token.ConsumerSecret, token.AccessToken, token.AccessSecret);
            var counter = 0;
            var apiResult = new Dto.UserInfoResult()
            {
                IsSucceed = false
            };

            while (counter < tryCount && !apiResult.IsSucceed)
            {
                try
                {
                    apiResult = api.GetUserInfo(userName, "");
                }
                catch (Exception exc)
                {
                }

                //token'ı yenile
                if (!apiResult.IsSucceed)
                {
                    token = GetRandomToken();
                    api = new Api.Gatherer(token.ConsumerKey, token.ConsumerSecret, token.AccessToken, token.AccessSecret);
                }

                counter += 1;
            }

            //donguden ciktiktan sonra hala isSucceed olmayabilir o zmn cevap vermesin
            return apiResult;
        }

        public Dto.TrendResult GetTrendsPlace(string woeid = "1", int tryCount = 3)
        {
            //tokenları alalım
            var token = GetRandomToken();
            var api = new Api.Gatherer(token.ConsumerKey, token.ConsumerSecret, token.AccessToken, token.AccessSecret);
            var counter = 0;
            var apiResult = new Dto.TrendResult()
            {
                IsSucceed = false
            };

            while (counter < tryCount && !apiResult.IsSucceed)
            {
                try
                {
                    apiResult = api.GetTrendsPlace(woeid);
                }
                catch
                {
                }

                //token'ı yenile
                if (!apiResult.IsSucceed)
                {
                    token = GetRandomToken();
                    api = new Api.Gatherer(token.ConsumerKey, token.ConsumerSecret, token.AccessToken, token.AccessSecret);
                }

                counter += 1;
            }

            //donguden ciktiktan sonra hala isSucceed olmayabilir o zmn cevap vermesin
            return apiResult;
        }
    }
}
